$.show = show;
$.hide = hide;

var parent,
isIPhoneX,
classes;

isIPhoneX = (OS_IOS && Ti.Platform.displayCaps.platformHeight === 812);

classes = {
  win: ['nlFokkezbToast_window'],
  view: ['nlFokkezbToast_view'],
  label: ['nlFokkezbToast_label'],
  animation: {
    enter: ['nlFokkezbToast_enterAnimation'],
    exit: ['nlFokkezbToast_exitAnimation']
  }
};

if (isIPhoneX){
  classes.win = ['nlFokkezbToast_window_iphonex'];
  classes.view = ['nlFokkezbToast_view_iphonex'];
  classes.animation.enter = ['nlFokkezbToast_enterAnimation_iphonex'];
  classes.animation.exit = ['nlFokkezbToast_exitAnimation_iphonex'];

  //Establecemos estilos iPhoneX Window
  $.resetClass($.window, classes.win);
}

(function constructor(args) {

  if (OS_ANDROID) {

    var properties = {
      message: args.message
    };

    if (args.duration) {

      // convert ms to constant
      if (args.duration !== Ti.UI.NOTIFICATION_DURATION_LONG && args.duration !== Ti.UI.NOTIFICATION_DURATION_SHORT) {
        properties.duration = (args.duration > 2000) ? Ti.UI.NOTIFICATION_DURATION_LONG : Ti.UI.NOTIFICATION_DURATION_SHORT;
      } else {
        properties.duration = args.duration;
      }
    }

    $.notification.applyProperties(properties);

    $.notification.show();

  } else {

    var viewClasses = classes.view;
    var labelClasses = classes.label;

    if (args.theme) {
      viewClasses.push('nlFokkezbToast_view_' + args.theme);
      labelClasses.push('nlFokkezbToast_label_' + args.theme);
    }

    $.resetClass($.view, viewClasses);
    $.resetClass($.label, labelClasses, {
      text: args.message
    });

    parent = args.view || $.window;
    parent.add($.view);

    if (!args.view) {
      $.window.open();
    }

    show();

    // set a timeout to hide and close
    if (!args.persistent) {
      setTimeout(function() {

        hide();

      }, args.duration || 3000);
    }
  }

})(arguments[0] || {});

function show(e) {

  // enterAnimation defined in TSS
  $.view.animate(_.omit($.createStyle({
    classes: classes.animation.enter
  }), 'classes'));

}

function hide(e) {

  // exitAnimation defined in TSS
  $.view.animate(_.omit($.createStyle({
    classes: classes.animation.exit

  }), 'classes'), function(e) {

    if (parent === $.window) {
      $.window.close();
    }

    parent.remove($.view);

  });
}
