/**
 * pushManager.js
 * Controlador push
 * @method  controller
 * @param   {object}   args
 */
(function controller(args) {
  var configuration,
    notifyUPVNotificationServer,
    pushManager,
    PUSH_DEVICE_TOKEN_KEYS;

  PUSH_DEVICE_TOKEN_KEYS = {
    DEVICE_TOKEN: 'deviceToken',
    OLD_DEVICE_TOKEN: 'oldDeviceToken'
  };

  configuration = args.configuration;

  //Funciones exportadas
  $.setup = setupPushManager;
  $.resetBadge = resetBadge;
  $.reset = resetController;

  notifyUPVNotificationServer = getNotifyUPVNotificationServer();

  pushManager = require('helpers/notification/pushManager');

  Alloy.Globals.eventManager.addListener('app:notifyUPVNotificationServer', {
    controller: 'pushManager',
    callback: notifyUPVNotificationServer
  });

  //Reseteamos el contador local de notificaciones en el icono de app
  resetBadge();

  /**
   * Inicializamos pushManager y obtenemos token de dispositivo
   * @method setupPushManager
   */
  function setupPushManager() {
    Ti.API.debug('setupPushManager on pushManager.js');

    if (canRegisterForPushNotifications() && initPushManager()) {
      Ti.API.debug('Request device token!');
      //Obtenemos token de dispositivo
      pushManager.retrieveDeviceToken();
    }
  }

  /**
   * Obtiene de la configuración la propiedad push
   * @method canRegisterForPushNotifications
   * @returns {Boolean}
   */
  function canRegisterForPushNotifications() {
    Ti.API.debug('canRegisterForPushNotifications on pushManager.js');

    var canRegisterForPushNotifications;

    //En la configuración de inicio viene en formato string '1'
    canRegisterForPushNotifications = Number(configuration.properties.push.enabled);

    return canRegisterForPushNotifications;
  }

  /**
   * Inicializa pushManager
   * @method initPushManager
   * @returns {boolean}
   */
  function initPushManager() {
    Ti.API.debug('initPushManager on pushManager.js');

    return pushManager.init({
      android: {
        showTrayNotificationsWhenFocused: true,
        focusAppOnPush: false,
        showAppOnTrayClick: true,
        showTrayNotification: true,
        singleCallback: true
      },
      language: Alloy.Globals.language,
      retrieveDeviceToken: {
        success: retrieveDeviceTokenSuccess,
        error: retrieveDeviceTokenError
      },
      channel: {
        subscribe: subscribeToChannelCallback,
        unsubscribe: unsubscribeToChannelCallback
      },
      receivePush: receivePush
    });
  }

  /**
   * Callback de éxito obtención token de dispositivo
   * @method retrieveDeviceTokenSuccess
   * @param {Object} e
   */
  function retrieveDeviceTokenSuccess(e) {
    Ti.API.debug('retrieveDeviceTokenSuccess on pushManager.js');

    var token;

    token = e.deviceToken;

    if (token) {
      manageDeviceToken(token);
    }
  }

  /**
   * Tratamos el token de dispositivo
   * @method manageDeviceToken
   * @param {Stringm} token
   */
  function manageDeviceToken(token) {
    Ti.API.debug('manageDeviceToken on pushManager.js');

    var channel,
      deviceToken,
      oldDeviceToken;

    channel = configuration.properties.push.channel;

    deviceToken = Ti.App.Properties.getString(PUSH_DEVICE_TOKEN_KEYS.DEVICE_TOKEN, null);
    oldDeviceToken = Ti.App.Properties.getString(PUSH_DEVICE_TOKEN_KEYS.OLD_DEVICE_TOKEN, null);

    //Si no tenemos token de dispositivo guardamos el token
    if (!deviceToken) {
      deviceToken = token;
      oldDeviceToken = token;
    } else if (token !== deviceToken) {
      //Si el nuevo token es diferente del almacenado actualizamos
      oldDeviceToken = deviceToken;
      deviceToken = token;
    }

    saveDeviceToken(deviceToken, oldDeviceToken);

    Ti.App.fireEvent('app:notifyUPVNotificationServer', {
      data: {
        token: {
          active: deviceToken,
          old: oldDeviceToken
        }
      }
    });

    subscribeToChannel(token, channel);
  }

  /**
   * Guardamos en properties los tokens
   * @method saveDeviceToken
   * @param {String} deviceToken
   * @param {String} oldDeviceToken
   */
  function saveDeviceToken(deviceToken, oldDeviceToken) {
    Ti.API.debug('saveDeviceToken on master.js');

    Ti.App.Properties.setString(PUSH_DEVICE_TOKEN_KEYS.DEVICE_TOKEN, deviceToken);
    Ti.App.Properties.setString(PUSH_DEVICE_TOKEN_KEYS.OLD_DEVICE_TOKEN, oldDeviceToken);
  }

  /**
   * Callback de error obtención token de dispositivo
   * @method retrieveDeviceTokenError
   * @param {Object} e
   */
  function retrieveDeviceTokenError(e) {
    Ti.API.debug('retrieveDeviceTokenError on master.js');
    Ti.API.debug('El registro del dispositivo ha fallado: ' + e.error);
  }

  /**
   * Callback que recibe la notificación push
   * @method receivePush
   * @param {Object} e
   */
  function receivePush(e) {
    Ti.API.debug('receivePush on master.js');
    resetBadge();
    Alloy.Globals.eventDispatcher.trigger('onPushNotification');
  }

  /**
   * Suscripción a canal de notificaciones
   * @method subscribeToChannel
   * @param {String} deviceToken
   * @param {String} channel
   */
  function subscribeToChannel(deviceToken, channel) {
    Ti.API.debug('subscribeToChannel on master.js', channel);

    pushManager.subscribeToChannel(deviceToken, channel);
  }

  /**
   * Desuscripción a canal de notificaciones
   * @method unsubscribeToChannel
   * @param {String} deviceToken
   * @param {String} channel
   */
  function unsubscribeToChannel(deviceToken, channel) {
    Ti.API.debug('unsubscribeToChannel on master.js', channel);

    pushManager.unsubscribeToChannel(deviceToken, channel);
  }

  /**
   * Callback que recibe el resultado de la suscripción a un canal
   * @method subscribeToChannelCallback
   * @param {Object} e
   */
  function subscribeToChannelCallback(e) {
    Ti.API.debug('subscribeToChannelCallback on master.js');

    if (e.success) {
      Ti.API.debug('Suscrito al canal');

      Ti.App.fireEvent('app:notifyUPVNotificationServer', {
        data: {
          suscriptionDone: true
        }
      });
    } else {
      Ti.API.debug('Error: ' + (e.error && e.message) || JSON.stringify(e));
    }
  }

  /**
   * Callback que recibe el resultado de la desuscripción a un canal
   * @method unsubscribeToChannelCallback
   * @param {Object} e
   */
  function unsubscribeToChannelCallback(e) {
    Ti.API.debug('unsubscribeToChannelCallback on master.js');
    if (e.success) {
      Ti.API.debug('Desuscrito del canal ');
    } else {
      Ti.API.debug('Error: ' + (e.error && e.message) || JSON.stringify(e));
    }
  }

  /**
   * Reset de badge en iOS
   * @method resetBadge
   */
  function resetBadge() {
    var deviceToken = Ti.App.Properties.getString(PUSH_DEVICE_TOKEN_KEYS.DEVICE_TOKEN, null);

    if (deviceToken) {
      pushManager.requestResetBadge(Ti.App.getId(), configuration.endPoint.resetBadge.url, configuration.properties.push.channel, deviceToken);
    }

    pushManager.resetBadge();
  }

  /**
   * Notifica al servidor de la upv con el token de dispostivo,
   * token de acceso, canal
   * @method notifyUPVNotificationServer
   * @returns {function}
   */
  function getNotifyUPVNotificationServer() {
    var _token,
      _count;

    _count = 2;
    _token = {
      active: null,
      old: null
    };

    return function notify(e) {
      Ti.API.debug('notifyUPVNotificationServer on master.js');

      var uuid,
        token,
        data,
        postData,
        HttpClient,
        httpClient,
        httpConfig;

      httpConfig = {};
      postData = {};
      data = e.data;

      /**
       * Callback de éxito tras notificación a servidor UPV
       * @method notifyUPVNotificationServerSuccess
       * @param {object} _httpClient
       */
      function notifyUPVNotificationServerSuccess(_httpClient) {
        Ti.API.debug('notifyUPVNotificationServerSuccess on master.js' + _httpClient.getResponseText());
      }

      /**
       * Callback de error tras notificación a servidor UPV
       * @method notifyUPVNotificationServerError
       * @param {object} _httpClient
       */
      function notifyUPVNotificationServerError(_httpClient) {
        Ti.API.debug('notifyUPVNotificationServerError on master.js' + _httpClient.getStatus());
      }

      if (data) {
        //Comprobamos datos de token de dispositivo
        if (data.token) {
          _count -= 1;
          _token.active = data.token.active;
          _token.old = data.token.old;
        }

        //Comprobamos datos de suscripción
        if (data.suscriptionDone) {
          _count -= 1;
        }
      }

      //@formatter:off
      if (_count === 0 && Alloy.Globals.upvLoginManager.private.isValidToken('access')) {
        //@formatter:on
        //make request to UPV Notification Server
        HttpClient = require('httpclient/httpClient');
        httpClient = new HttpClient();

        token = Alloy.Globals.upvLoginManager.private.getAccessTokenFromProperties();
        uuid = Alloy.Globals.upvLoginManager.private.getUUID();

        postData = {
          p_appid: Ti.App.getId(),
          p_version: Ti.App.getVersion(),
          p_devicetoken: _token.active,
          p_channel: configuration.properties.push.channel
        };

        if (_token.active !== _token.old) {
          postData.p_expireddevicetoken = _token.old;
        }

        httpConfig = {
          method: 'POST',
          url: configuration.endPoint.upvPushRegister.url,
          success: notifyUPVNotificationServerSuccess,
          error: notifyUPVNotificationServerError,
          timeout: configuration.properties.timeout || Alloy.CFG.webservice.properties.timeout,
          headers: {
            Authorization: 'Bearer ' + token + ':' + uuid
          },
          data: postData
        };

        if (httpClient.init(httpConfig)) {
          httpClient.doRequest();
        }
      }
    };
  }

  /**
   * Limpiamos controlador
   * @method resetController
   */
  function resetController() {
    //Eliminamos eventos de controlador
    Alloy.Globals.eventManager.removeListenerFromController('app:notifyUPVNotificationServer', 'pushManager');

    pushManager = null;

    $.removeListener();
    $.off();
    $.destroy();
  }

})(arguments[0] || {});
