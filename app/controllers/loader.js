/**
 * Controlador loader de inicio
 * loader.js
 */
(function controller(args) {
  'use strict';

  //Funciones exportadas
  $.setAnimationWidth = setAnimationWidth;
  $.setAnimation = setAnimation;
  $.setMessage = setMessage;
  $.reset = resetController;

  /**
   * Inicializamos controlador
   * @method  onCreateController
   */
  (function onCreateController() {
    if (args.bg) {
      $.loader.backgroundColor = args.bg;
    }

    if (args.animation) {
      setAnimationParams(args.animation);
    }

    if (args.message) {
      setMessage(args.message);
    }
  })();

  /**
   * Establece propiedades de Lottie
   * @method setAnimationParams
   * @param  {object}        params
   */
  function setAnimationParams(params) {
    if (params.width) {
      setAnimationWidth(params.width);
    }

    if (params.file) {
      setAnimation(params.file);
    }
  }

  /**
   * Establece tamaños width de loader
   * @method setAnimationWidth
   * @param {String} width
   */
  function setAnimationWidth(width) {
    $.lottie.width = width;
  }

  /**
   * Establece animación de loader
   * @method setAnimation
   * @param {String} file
   */
  function setAnimation(file) {
    $.lottie.file = file;
  }

  /**
   * Establece mensaje de loader
   * @method setMessage
   * @param {String} message
   */
  function setMessage(message) {
    $.message.setText(message);
  }

  /**
   * resetController
   * @description Eliminamos eventos y liberamos memoria
   */
  function resetController() {
    //Eliminamos eventos de controlador
    $.removeListener();
    $.off();
    $.destroy();
  }

})(arguments[0] || {});
