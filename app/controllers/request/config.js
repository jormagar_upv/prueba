/**
 * config.js
 * Controlador recupera configuración remota
 * @method  controller
 * @param   {object}   args
 */
(function controller(args) {
  'use strict';

  const CONFIGURATION_KEYS = {
    DATA: 'configuration',
    CACHE_TTL: 'configurationTTL'
  };

  const API_KEYS = args.apiKeys || Object.keys(Alloy.CFG.webservice.endPoint);

  const VersionManager = {
    values: {
      url: null
    },
    get url() {
      return this.values.url;
    },
    set url(value) {
      this.values.url = value;
    }
  };

  const DataManager = {
    Network: new Manager.Network(),
    Cache: new Manager.Cache()
  };

  //Funciones exportadas
  $.isCacheExpired = isCacheExpired;
  $.isObsoleteVersion = isObsoleteVersion;

  $.getRemoteConfiguration = getRemoteConfiguration;
  $.getNewVersionUrl = getNewVersionUrl;
  $.getConfiguration = getConfiguration;

  $.cleanCache = cleanCache;
  $.reset = reset;

  /**
   * getRemoteConfiguration
   * @description Obtenemos la configuración inicial de la app
   */
  function getRemoteConfiguration() {
    Ti.API.debug('getRemoteConfig on config.js');

    const httpConfig = {
      method: 'GET',
      url: Alloy.CFG.webservice.configuration.url,
      success: getRemoteConfigurationSuccess,
      error: getRemoteConfigurationError,
      timeout: Alloy.CFG.webservice.properties.timeout || 3000
    };

    const result = DataManager.Network.getRemoteData(httpConfig);

    if (!result) {
      getRemoteConfigurationError();
    }
  }

  /**
   * getRemoteConfigurationSuccess
   * @description Callback de éxito
   * @param {Object} httpClient
   */
  function getRemoteConfigurationSuccess(httpClient) {
    Ti.API.debug('getRemoteConfigurationSuccess on config.js');

    let normalizedConfig;

    try {
      normalizedConfig = normalizeRemoteConfig(JSON.parse(httpClient.getResponseText()));
    } catch (e) {
      normalizedConfig = getDefaultConfiguration();
    } finally {
      saveConfiguration(normalizedConfig);
      done();
    }
  }

  /**
   * getRemoteConfigurationError
   * @description Callback de fracaso
   * @param {Object} httpClient
   */
  function getRemoteConfigurationError(httpClient) {
    Ti.API.debug('getRemoteConfigurationError on config.js');

    saveConfiguration(getDefaultConfiguration());
    done();
  }

  /**
   * isValidRemoteConfig
   * @description Valida una configuración remota
   * @param {Object} remoteConfig
   * @returns {Object}
   */
  function isValidRemoteConfig(remoteConfig) {
    let isValidConfig = true;

    if (!Object(remoteConfig).parametrosDefecto) {
      isValidConfig = false;
    }

    if (!Array.isArray(remoteConfig.parametrosDefecto)) {
      isValidConfig = false;
    }

    if (!Object(remoteConfig).serviciosApiRest) {
      isValidConfig = false;
    }

    return isValidConfig;
  }

  /**
   * getDefaultConfiguration
   * @description Obtiene la configuración remota por defecto
   * @returns {Object} defaultConfiguration
   */
  function getDefaultConfiguration() {
    Ti.API.debug('getDefaultConfiguration on config.js');

    return JSON.parse(JSON.stringify(Alloy.CFG.webservice));
  }

  /**
   * normalizeRemoteConfig
   * @description Normaliza los datos remotos
   * @param {Object} remoteConfig
   * @returns {Object}
   */
  function normalizeRemoteConfig(remoteConfig) {
    Ti.API.debug('normalizeRemoteConfig on config.js');

    let config = getDefaultConfiguration();

    if (isValidRemoteConfig(remoteConfig)) {
      //Parámetros remotos por defecto
      remoteConfig.parametrosDefecto.forEach(function iterator(element) {

        if (element.valor) {
          if (element.propiedad === 'timeout') {
            config.properties.timeout = Number(element.valor) * 1000;
          } else if (element.propiedad === 'urlUpvLogin') {
            config.properties.upvLogin.url = String(element.valor);
          } else if (element.propiedad === 'obsoleta') {
            config.properties.obsolete.url = String(element.valor);
          } else if (element.propiedad === 'push') {
            config.properties.push.enabled = Number(element.valor);
          } else if (element.propiedad === 'channel') {
            config.properties.push.channel = String(element.valor);
          }
        }
      });

      //Servicios API (serviciosApiRest)
      remoteConfig.serviciosApiRest.forEach(function iterator(element) {
        if (element.URI) {
          if (API_KEYS.indexOf(element.servicio) !== -1) {

            config.endPoint[element.servicio].url = String(element.URI);

            //Si viene TTL lo establecemos, si no, es 0
            if (element.ttl) {
              config.endPoint[element.servicio].ttl = Number(element.ttl);
            }
          }
        }
      });
    }

    //Force obsolete version
    //config.properties.obsolete.url = 'https://www.upv.es';

    return config;
  }

  /**
   * saveConfiguration
   * @description Guarda en properties la configuración de la aplicación
   * @param {Object} config Configuración de la aplicación
   */
  function saveConfiguration(config) {
    Ti.API.debug('saveConfiguration on config.js' + JSON.stringify(config, null, 2));

    DataManager.Cache.setDataToProperties('object', CONFIGURATION_KEYS.DATA, config);
    DataManager.Cache.setNextTTLForCache(CONFIGURATION_KEYS.CACHE_TTL, Alloy.CFG.webservice.configuration.ttl);
  }

  /**
   * done
   * @description Ejecuta el callback del controlador padre
   * continuando la ejecución
   */
  function done() {
    Ti.API.debug('done on config.js');

    $.trigger('onRemoteConfigCompleted');
  }

  /**
   * getConfiguration
   * @description Método de consulta externa, devuelve la configuración
   * @returns {Object}
   */
  function getConfiguration() {
    return DataManager.Cache.getDataFromProperties('object', CONFIGURATION_KEYS.DATA);
  }

  /**
   * isCacheExpired
   * @description Método de consulta externa
   * @returns {Boolean}
   */
  function isCacheExpired() {
    return DataManager.Cache.isExpired(CONFIGURATION_KEYS.CACHE_TTL);
  }

  /**
   * getNewVersionUrl
   * @description Obtiene la url de descarga de la nueva versión
   * @returns {string}
   */
  function getNewVersionUrl() {
    return VersionManager.url;
  }

  /**
   * isObsoleteVersion
   * @description Comprueba que la aplicación sea obsoleta
   * @returns {Boolean}
   */
  function isObsoleteVersion() {
    Ti.API.debug('isObsoleteVersion on config.js');

    const configuration = getConfiguration();

    let isObsolete = false;

    //Force Obsolete
    //configuration.obsoleteVersion = 'http://google.com';

    if (configuration.properties.obsolete.url) {

      isObsolete = true;

      VersionManager.url = configuration.properties.obsolete.url;

      //Limpiamos la caché de la config ya que tras un update de la app no
      // se eliminan las properties.
      cleanCache();
    }

    return isObsolete;
  }

  /**
   * cleanCache
   * @description Limpiamos caché de configuración remota
   */
  function cleanCache() {
    DataManager.Cache.clean(CONFIGURATION_KEYS.DATA);
    DataManager.Cache.clean(CONFIGURATION_KEYS.CACHE_TTL);
  }

  /**
   * Limpieza de controlador
   * @method reset
   */
  function reset() {
    $.removeListener();
    $.off();
    $.destroy();
  }

})(arguments[0] || {});
