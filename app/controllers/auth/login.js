/**
 * Controlador de inicio de sesión
 * @method  controller
 * @param   {object}   args
 */
(function controller(args) {
  'use strict';

  /**
   * Inicializamos controlador
   * @method  onCreateController
   */
  (function onCreateController() {
    addListener();
  })();

  /**
   * Añadimos manejadores de eventos
   * @method  addListener
   */
  function addListener() {
    Ti.API.debug('bindEvents');

    //Eventos de UI
    $.addListener($.login, 'click', onLoginClick);
    $.addListener($.password, 'change', onPasswordChange);
    $.addListener($.showPassword, 'click', onShowPasswordClick);
  }

  /**
   * Callback click botón login
   * @method onLoginClick
   * @param  {object}     e
   */
  function onLoginClick(e) {
    Helper.Toast.show('Login');
  }

  /**
   * Callback al escribir sobre el campo contraseña
   * @method onPasswordChange
   * @param  {object}         e
   */
  function onPasswordChange(e) {
    $.showPassword.setVisible(e.source.hasText());
  }

  /**
   * Callback click en mostrar contraseña
   * @method onShowPasswordClick
   * @param  {object}         e
   */
  function onShowPasswordClick(e) {
    const mask = !$.password.passwordMask;
    let eyeIcon = '\uECB5';

    if (mask) {
      eyeIcon = '\uECB4';
    }

    $.password.setPasswordMask(mask);
    $.showPassword.setText(eyeIcon);

  }

})(arguments[0] || {});
