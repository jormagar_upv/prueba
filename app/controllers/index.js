/**
 * Controlador de inicio
 * @method  controller
 * @param   {object}   args
 */
(function controller(args) {
  'use strict';

  const win = OS_IOS ? 'nav' : 'win';

  const Controller = {
    Config: null,
    Permissions: null,
    Login: null,
    Profile: null
  };

  //Controlador UI activo
  const ActiveController = {
    Permissions: false,
    Login: false,
    Profile: false
  };

  Controller.Config = Alloy.createController('request/config');

  /**
   * Inicializamos controlador
   * @method  onCreateController
   */
  (function onCreateController() {
    addListener();

    $[win].open();
  })();

  /**
   * Añadimos manejadores de eventos
   * @method  addListener
   */
  function addListener() {
    Ti.API.debug('bindEvents');

    //Eventos de controladores
    Controller.Config.on('onRemoteConfigCompleted', runApplication);

    //Eventos de UI
    $.addListener($.win, 'open', onOpen);
    $.addListener($.win, 'close', onClose);

    //Eventos de plataforma
    if (OS_ANDROID) {
      Ti.Android.currentActivity.addEventListener('newintent', function onNewIntent(e) {
        Ti.API.debug('onNewIntent');
      });
    }
  }

  /**
   * Callback evento open
   * @method  onOpen
   * @param   {object} e
   */
  function onOpen(e) {
    Ti.API.debug('index onOpen');
    _.delay(checkRemoteConfigCache, 100);
  }

  /**
   * Callback evento close
   * @method  onClose
   * @param   {object} e
   */
  function onClose(e) {
    //reset();
  }

  /**
   * Comprobamos caché de configuración inicial
   * @method  checkRemoteConfigCache
   * @param   {object}               e
   */
  function checkRemoteConfigCache(e) {
    if (Controller.Config.isCacheExpired()) {
      Controller.Config.getRemoteConfiguration();
    } else {
      runApplication();
    }
  }

  /**
   * Lanza la aplicación
   * @method runApplication
   */
  function runApplication() {
    if (Controller.Config.isObsoleteVersion()) {
      createObsoleteController();
    } else if (OS_IOS) {
      //checkAppArguments();
      checkLoginStatus();
    } else if (OS_ANDROID) {
      checkPermissions();
    }
  }

  /**
   * Añade a la ventana la vista del controlador obsolete
   * @method createObsoleteController
   */
  function createObsoleteController() {
    //Al ser pantalla final, lo inyectamos directamente
    Helper.Fade.setContent($.container, Alloy.createController('obsolete', {
      url: Controller.Config.getNewVersionUrl()
    }).getView(), true);
  }

  /**
   * Comprueba los permisos de usuario
   * @method checkPermissions
   */
  function checkPermissions() {
    if (!hasPermissions()) {
      createPermissionController();
    } else {
      checkLoginStatus();
    }
  }

  /**
   * Comprueba los permisos de almacenamiento
   * @method hasPermissions
   * @returns {boolean}
   */
  function hasPermissions() {
    return Ti.Android.hasPermission('android.permission.WRITE_EXTERNAL_STORAGE');
  }

  /**
   * Añade a la ventana la vista del controlador de permisos
   * @method createPermissionController
   */
  function createPermissionController() {
    if (Controller.Permissions === null) {

      Controller.Permissions = Alloy.createController('permissions', {
        activity: $.win.activity
      });

      Controller.Permissions.on('permissionsGranted', permissionsGranted);
    }

    if (!ActiveController.Permissions) {
      ActiveController.Permissions = true;

      Helper.Fade.setContent($.container, Controller.Permissions.getView(), true);
    }

    resetControllers(['Login', 'Profile']);
  }

  /**
   * permissionsGranted
   * @description Callback al obtener permisos
   * @param {Object} e
   */
  function permissionsGranted(e) {
    //setAppLifecycleEventsForAndroid();
    checkLoginStatus();
  }

  /**
   * Comprueba el estado del login
   * @method checkLoginStatus
   */
  function checkLoginStatus() {
    if (Manager.Token.isExpired()) {
      createLoginController();
    } else {
      createProfileController();
    }
  }

  /**
   * Añade a la ventana la vista del controlador de login
   * @method createLoginController
   */
  function createLoginController() {
    if (Controller.Login === null) {

      Controller.Login = Alloy.createController('auth/login', {
        activity: $.win.activity
      });
    }

    if (!ActiveController.Login) {
      ActiveController.Login = true;

      Helper.Fade.setContent($.container, Controller.Login.getView(), true);
    }

    resetControllers(['Permissions', 'Profile']);
  }

  /**
   * Añade a la ventana la vista del controlador de perfil autenticado
   * @method createProfileController
   */
  function createProfileController() {
    if (Controller.Profile === null) {

      Controller.Profile = Alloy.createController('auth/profile', {
        activity: $.win.activity
      });
    }

    if (!ActiveController.Profile) {
      ActiveController.Profile = true;

      Helper.Fade.setContent($.container, Controller.Profile.getView(), true);
    }

    resetControllers(['Permissions', 'Login']);
  }

  /**
   * Ejecuta la función de limpieza de los controladores proporcionados
   * @method resetControllers
   * @param  {object}            list Lista de controladores a limpiar
   */
  function resetControllers(list) {
    list.forEach(function (key) {
      //Si existe el controlador, lo reseteamos
      if (Controller[key]) {
        Controller[key].reset();
        Controller[key] = null;
      }
      ActiveController[key] = false;
    });
  }

  /**
   * Limpieza de controlador
   * @method reset
   */
  function reset() {
    //Estos eventos globales son reseteados al recibir una nueva invocación,
    //sin embargo debemos resetear los controladores activos

    //Eliminamos eventos globales
    //Events.Global.removeListenerFromController('uncaughtexception', 'index');;

    //Eliminamos eventos de plataforma
    //Ti.Android.currentActivity.removeEventListener('newintent', onNewIntent);

    //Limpiamos subcontroladores
    Controller.Config.off('onRemoteConfigCompleted', runApplication);

    resetControllers(['Config', 'Permissions', 'Login', 'Profile']);

    //Limpiamos controlador
    $.removeListener();
    $.off();
    $.destroy();
  }

  /**
   * Callback excepción no controlada
   * @method  onUncaughtException
   * @param   {object}            e
   */
  function onUncaughtException(e) {
    Ti.API.error(JSON.stringify(e, null, 2));
  }

})(arguments[0] || {});
