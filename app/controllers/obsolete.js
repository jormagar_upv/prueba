/**
 * obsolete.js
 * Controlador Obsolete
 * @method  controller
 * @param   {object}   args
 */
(function controller(args) {

  'use strict';

  var HEADER_HEIGHT,
    url,
    debouncedOnCloseDetail,
    debouncedOpenUrl;

  HEADER_HEIGHT = Alloy.Globals.device.width * (9 / 16);

  url = args.url;

  debouncedOnCloseDetail = _.debounce(onCloseDetail, 1500, true);
  debouncedOpenUrl = _.debounce(openUrl, 1500, true);

  /**
   * Establece los eventos del controlador
   * @method bindEvents
   */
  (function bindEvents() {
    $.addListener($.download, 'click', debouncedOpenUrl);
    $.addListener($.content, 'swipe', onSwipe);
  })();

  /**
   * Callback de creación de controlador
   * @method onCreateController
   */
  (function onCreateController() {
    setLayout();
  })();

  /**
   * Establece el layout de la página
   * @method setLayout
   */
  function setLayout() {
    $.header.setWidth(Alloy.Globals.device.width);
    $.header.setHeight(HEADER_HEIGHT);
    $.content.setTop(HEADER_HEIGHT);
  }

  /**
   * Abre la url de descarga de
   * la nueva versión
   * @method openUrl
   * @param {object} e
   */
  function openUrl(e) {
    if (url) {
      Ti.Platform.openURL(url);
    }
  }

  /**
   * Callback swipe
   * @method onSwipe
   * @param  {object}     e
   */
  function onSwipe(e) {
    var swipeUpAnimation,
      titleAnimation,
      closeAnimation;

    if (e.direction === 'up') {
      $.removeListener($.content, 'swipe', onSwipe);

      swipeUpAnimation = Ti.UI.createAnimation({
        top: 0,
        duration: 500
      });

      closeAnimation = Ti.UI.createAnimation({
        opacity: 1,
        duration: 500
      });

      titleAnimation = Ti.UI.createAnimation({
        left: 72,
        duration: 500
      });

      swipeUpAnimation.addEventListener('complete', onCompleteSwipeUp);
      closeAnimation.addEventListener('complete', onCompleteCloseAnimation);
      titleAnimation.addEventListener('complete', onCompleteTitleAnimation);

      $.content.animate(swipeUpAnimation);
      $.title.animate(titleAnimation);
      $.closeDetail.animate(closeAnimation);
    }
  }

  /**
   * Callback fin animación swipe up
   * @method  onCompleteSwipeUp
   * @param   {object}          e
   */
  function onCompleteSwipeUp(e) {
    e.source.removeEventListener('complete', onCompleteSwipeUp);
    $.content.scrollingEnabled = true;

    $.addListener($.closeDetail, 'click', debouncedOnCloseDetail);
  }

  /**
   * Callback fin animación title
   * @method  onCompleteTitleAnimation
   * @param   {object}          e
   */
  function onCompleteTitleAnimation(e) {
    e.source.removeEventListener('complete', onCompleteTitleAnimation);
  }

  /**
   * Callback fin animación
   * @method  onCompleteCloseAnimation
   * @param   {object}             e
   */
  function onCompleteCloseAnimation(e) {
    e.source.removeEventListener('complete', onCompleteCloseAnimation);
  }

  /**
   * Callback close detail
   * @method  onCloseDetail
   * @param   {object}    e
   */
  function onCloseDetail(e) {
    var swipeDownAnimation,
      titleAnimation,
      closeAnimation;

    swipeDownAnimation = Ti.UI.createAnimation({
      top: HEADER_HEIGHT,
      duration: 500
    });

    titleAnimation = Ti.UI.createAnimation({
      left: 16,
      duration: 500
    });

    closeAnimation = Ti.UI.createAnimation({
      opacity: 0,
      duration: 500
    });

    swipeDownAnimation.addEventListener('complete', onCompleteSwipeDown);
    titleAnimation.addEventListener('complete', onCompleteTitleAnimation);
    closeAnimation.addEventListener('complete', onCompleteCloseAnimation);

    $.content.animate(swipeDownAnimation);
    $.title.animate(titleAnimation);
    $.closeDetail.animate(closeAnimation);
  }

  /**
   * Callback fin animación swipe down
   * @method  onCompleteSwipeDown
   * @param   {object}            e
   */
  function onCompleteSwipeDown(e) {
    Ti.API.debug('onCompleteSwipeDown');

    $.content.scrollingEnabled = false;
    //$.content.zIndex = 1;

    e.source.removeEventListener('complete', onCompleteSwipeDown);

    $.removeListener($.closeDetail, 'click', debouncedOnCloseDetail);
    $.addListener($.content, 'swipe', onSwipe);
  }

})(arguments[0] || {});
