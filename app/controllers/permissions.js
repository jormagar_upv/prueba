/**
 * permissions.js
 * Controlador gestión permisos
 * @method  controller
 * @param   {object}    args
 */
(function controller(args) {
  'use strict';

  const ASK_PERMISSIONS_PAGE = 0;
  const DENIED_PERMISSIONS_PAGE = 1;
  const CHECK_PERMISSIONS_PAGE = 2;
  const EXTERNAL_STORAGE_PERMISSION = 'android.permission.WRITE_EXTERNAL_STORAGE';

  const activity = args.activity;

  const permissionManager = require('helpers/permissionManager');

  const debouncedRequestPermissions = _.debounce(requestPermissions, 1500, true);
  const debouncedOpenPermissions = _.debounce(openPermissions, 1500, true);
  const debouncedExit = _.debounce(exit, 1500, true);

  //Funciones exportadas
  $.reset = reset;

  /**
   * Inicializamos controlador
   * @method onCreateController
   */
  (function onCreateController() {
    Ti.API.debug('onCreateController on permissions.js');

    $.permissionPager.setShowPagingControl(false);
    $.permissionPager.setScrollingEnabled(false);
    $.permissionPager.scrollToView(ASK_PERMISSIONS_PAGE);
  })();

  /**
   * Añadimos manejadores de eventos
   * @method  addListener
   */
  (function addListener() {
    //Eventos de UI
    $.addListener($.allowPermissions, 'click', debouncedRequestPermissions);
    $.addListener($.openPermissions, 'click', debouncedOpenPermissions);
    $.addListener($.exit, 'click', debouncedExit);

    //Anulamos ciclo de vida Android
    activity.onResume = null;
    activity.onStop = null;
  })();

  /**
   * Solicita permisos
   * @method requestPermissions
   * @param {object} e
   */
  function requestPermissions(e) {
    Ti.API.debug('requestPermissions on permissions.js');

    permissionManager.request('storage', success, error);
  }

  /**
   * Restablecemos eventos del padre y
   * lanzamos evento
   * @method success
   */
  function success() {
    Ti.API.debug('success on permissions.js');

    $.trigger('permissionsGranted');
  }

  /**
   * Callback error permisos
   * @method error
   * @param {Object} e
   */
  function error(e) {
    $.permissionPager.scrollToView(DENIED_PERMISSIONS_PAGE);
  }

  /**
   * Abre pagina permisos de app
   * @method openPermissions
   * @param {Object} e
   */
  function openPermissions(e) {
    Ti.API.debug('openPermissions on permission.js');

    permissionManager.openAppPermissions();
    activity.onResume = hasPermissions;
  }

  /**
   * Comprueba que existan permisos
   * @method hasPermissions
   */
  function hasPermissions() {
    Ti.API.debug('hasPermissions on permission.js');

    if (Ti.Android.hasPermission(EXTERNAL_STORAGE_PERMISSION)) {
      success();
    }
  }

  /**
   * Termina la aplicación
   * @method exit
   * @param {Object} e
   */
  function exit(e) {
    Ti.API.debug('exit on permission.js');

    activity.finish();
  }

  /**
   * Limpia el controlador
   * @method reset
   */
  function reset() {
    Ti.API.debug('reset on permission.js');

    $.removeListener();
    $.off();
    $.destroy();
  }

})(arguments[0] || {});
