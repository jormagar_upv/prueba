'use strict';

let iOSAppEventHandler;

//Requerimos paquetes de funcionalidad

//Objeto helper
const Helper = {
  //Helper Loader
  Loader: Alloy.createWidget('com.caffeinalab.titanium.loader', {
    message: L('loading', 'Cargando'),
    cancelable: false,
    useImages: false
  }),
  //Helper toast
  Toast: Alloy.createWidget('nl.fokkezb.toast', 'global'),
  //Helper Controller Fades
  Fade: require('helpers/ui/fade')
};

const Manager = {
  //Network Manager
  Network: require('helpers/network/networkManager'),
  //Cache Manager
  Cache: require('helpers/cache/cacheManager'),
  //Manager librería upvlogin
  Token: new (require('login/TokenManager'))(),
  //Manager PackageManager
  PackageManager: new (require('helpers/PackageManager'))(),
  //Manager notificaciones push
  Push: require('helpers/notification/pushManager')
};

const Events = {
  //Helper que amplía la funcionalidad de Ti.App.addEventListener
  Global: require('helpers/eventManager'),
  //Helper que nos brinda la funcionalidad de los Eventos de Backbone
  Dispatcher: _.clone(Backbone.Events),
  //Helper registro de logs remotos
  Log: require('helpers/logEvents')
};

//Variables
Alloy.Globals.UI = require('helpers/ui/metrics');

Ti.API.debug(JSON.stringify(Alloy.Globals.UI, null, 2));

//Variables plataformas
Alloy.Globals.appInBackground = null;
Alloy.Globals.iPhoneX = (OS_IOS && Ti.Platform.displayCaps.platformHeight >= 812);

//Obtenemos la url de configuración remota
Alloy.CFG.webservice.configuration.url = (function getConfigurationUrl(value) {
  var url,
    platform;

  url = value;
  platform = (OS_IOS) ? 'ios' : 'android';

  url = url.replace('{app_id}', Ti.App.getId())
    .replace('{platform}', platform)
    .replace('{app_version}', Ti.App.getVersion());

  return url;

})(Alloy.CFG.webservice.configuration.url);

/**
 * Devuelve código ISO 2-letras del lenguaje actual.
 * Por defecto devuelve el idioma inglés.
 * @method getCurrentLanguage
 * @return {string}
 */
Alloy.Globals.language = (function getCurrentLanguage() {
  var lang;
  switch (Titanium.Locale.getCurrentLanguage().substr(0, 2)) {
    case 'en':
      lang = 'en';
      break;
    case 'ca':
      lang = 'ca';
      break;
    case 'es':
      lang = 'es';
      break;
    default:
      lang = 'en';
      break;
  }
  return lang;
})();
