/**
 * Helper TokenManager
 * @author Jorge Macías García
 * @copyright Universitat Politècnica de València (c) 2018
 * @module TokenManager
 * @version 1.0.0
 */

module.exports = (function () {

  'use strict';

  const CONFIG = {
    APP_ID: Ti.App.getId(),
    APP_GROUP: 'group.es.upv.asic.share',
    TOKEN_FILENAME: 'token.json',
    SECRET: {
      PART_1: 'pKaHgsbmsi',
      PART_2: 'ugmwonfSAa',
      PART_3: 'KMvx5EOPjY'
    }
  };

  /**
   * Función auxiliar de validación de objetos
   * @method  validate
   * @param   {object} required Objeto descriptor de campos requeridos
   * @param   {object} obj      Objeto de entrada a analizar
   * @returns {boolean}
   */
  function validate(required, obj) {
    return required.every(function (element) {
      return (typeof this[element.key] === element.type) && (!element.required || validate(element.required, this[element.key]));
    }, Object(obj));
  }

  const Library = {
    NetworkManager: require('helpers/network/networkManager')
  };

  const TOKEN_TYPES = {
    REFRESH: 'refresh',
    ACCESS: 'access'
  };

  const CryptoJS = require('login/cryptojs');
  const NetworkManager = new Library.NetworkManager();

  /**
   * Constructor librería TokenManager
   * @constructor TokenManager
   */
  function TokenManager() {
    Ti.API.debug('TokenManager.js _ constructor');

    this.isExternalStoragePresent = Ti.Filesystem.isExternalStoragePresent();
    this.token = null;
    this.ttl = null;
    this.callback = {
      success: null,
      error: null
    };
  }

  TokenManager.prototype.setCallbacks = function (callbacks) {
    Ti.API.debug('TokenManager.js _ setCallbacks');

    if (Object(callbacks).hasOwnProperty('success') &&
      Object(callbacks).hasOwnProperty('error')) {
      this.callback.success = callbacks.success;
      this.callback.error = callbacks.error;
    } else {
      throw new Error('TokenManager.setCallbacks: Error estableciendo callbacks');
    }
  };

  /**
   * Realiza un login
   * @method request
   * @param {Object} config
   * @returns {Boolean}
   */
  TokenManager.prototype.login = function (config) {
    Ti.API.debug('TokenManager _ login');
    this.setCallbacks({
      success: config.success,
      error: config.error
    });

    return this.request(config);
  };

  /**
   * Realiza logout anulando token de refresco
   * @method logout
   * @param {Object} config
   * @returns {Boolean}
   */
  TokenManager.prototype.logout = function (config) {
    Ti.API.debug('TokenManager _ logout');

    this.setCallbacks({
      success: config.success,
      error: config.error
    });
    return this.request(config);
  };

  /**
   * Solicitamos un token
   * @method requestToken
   * @param   {object} params
   */
  TokenManager.prototype.requestToken = function (params) {
    Ti.API.debug('TokenManager _ requestToken');

    //Validar entrada
    this.setCallbacks({
      success: params.config.success,
      error: params.config.error
    });

    switch (params.type) {
      case TOKEN_TYPES.REFRESH:
        this.requestRefreshToken(params.config);
        break;
      case TOKEN_TYPES.ACCESS:
        this.requestAccessToken(params.config);
        break;
      default:
        throw new Error('TokenManager.requestToken: Tipo de token incorrecto');
    }
  };

  /**
   * Obtiene un token de refresco
   * @method requestRefreshToken
   * @param {Object} config
   * @returns {Boolean}
   */
  TokenManager.prototype.requestRefreshToken = function (config) {
    Ti.API.debug('TokenManager.requestRefreshToken');

    return this.request(config);
  };

  /**
   * Obtiene un token de acceso
   * @method requestAccessToken
   * @param {Object} config
   * @returns {Boolean}
   */
  TokenManager.prototype.requestAccessToken = function (config) {
    Ti.API.debug('TokenManager.requestAccessToken');

    return this.request(config);
  };

  /**
   * Realiza una peticion
   * @method request
   * @param {Object} config
   * @returns {boolean} result
   */
  TokenManager.prototype.request = function (config) {
    Ti.API.debug('TokenManager.request');

    let result = false;

    config.success = this.success.bind(this);
    config.error = this.error.bind(this);

    result = NetworkManager.getRemoteData(config);

    return result;
  };

  /**
   * Callback exito request
   * @method success
   * @param {Object} httpClient
   */
  TokenManager.prototype.success = function (httpClient) {
    Ti.API.debug('TokenManager.success');

    this.callback.success(httpClient);
  };

  /**
   * Callback error request
   * @method error
   * @param {Object} httpClient
   */
  TokenManager.prototype.error = function (httpClient) {
    Ti.API.debug('TokenManager.error');

    this.callback.error(httpClient);
  };

  /**
   * Leemos fichero de token
   * @method readFile
   * @param {string} filename
   * @returns {string} data
   */
  TokenManager.prototype.readFile = function (filename) {
    Ti.API.debug('TokenManager.readFile');

    let data;

    const sharedPath = this.getSharedPath();

    if (sharedPath) {
      const sharedFile = Ti.Filesystem.getFile(sharedPath, filename);

      if (sharedFile.exists()) {
        const decryptedData = CryptoJS.TripleDES.decrypt(sharedFile.read().text, this.getSecret());
        data = decryptedData.toString(CryptoJS.enc.Utf8);

        if (data) {
          data = JSON.parse(data);
        }
      }
    }

    return data;
  };

  /**
   * Almacena el token, ttl y uuid en un archivo privado o compartido
   * @method writeFile
   * @param {Object} data
   * @param {String} filename
   */
  TokenManager.prototype.writeFile = function (data, filename) {
    Ti.API.debug('TokenManager.writeFile');

    const sharedPath = this.getSharedPath();

    if (sharedPath) {

      const sharedFile = Ti.Filesystem.getFile(sharedPath, filename);

      if (OS_IOS) {
        //Si no existe el fichero lo crea
        sharedFile.createFile();
      }

      const encryptedData = CryptoJS.TripleDES.encrypt(JSON.stringify(data), this.getSecret());

      sharedFile.write(encryptedData.toString());
      sharedFile = null;
    }
  };

  /**
   * Elimina el fichero de token
   * @method deleteFile
   * @param {string} filename
   */
  TokenManager.prototype.deleteFile = function (filename) {
    Ti.API.debug('TokenManager.deleteFile');

    const sharedPath = this.getSharedPath();

    if (sharedPath) {
      const sharedFile = Ti.Filesystem.getFile(sharedPath, filename);
      sharedFile.deleteFile();
    }
  };

  /**
   * Devuelve el secret para la codificación/decodificación
   * del fichero de token
   * @method getSecret
   * @returns {string} secret
   */
  TokenManager.prototype.getSecret = function () {
    Ti.API.debug('TokenManager.getSecret');

    let secret;

    secret = CONFIG.SECRET.PART_1;
    secret += '()';
    secret += CONFIG.SECRET.PART_2;
    secret += '()';
    secret += CONFIG.SECRET.PART_3;
    secret = Titanium.Utils.sha1(secret);

    return secret;
  };

  /**
   * Obtiene el path indicado para leer / guardar el Token
   * @method getSharedPath
   * @returns {String}
   */
  TokenManager.prototype.getSharedPath = function () {
    Ti.API.debug('TokenManager.getSharedPath');

    let sharedPath;

    if (OS_IOS) {
      sharedPath = this.getSharedPathForIOS();
    } else if (OS_ANDROID) {
      sharedPath = this.getSharedPathForAndroid();
    }

    return sharedPath;
  };

  /**
   * Obtiene el directorio privado de IOS
   * @method getSharedPathForIOS
   * @returns {string}
   */
  TokenManager.prototype.getSharedPathForIOS = function () {
    Ti.API.debug('TokenManager.getSharedPathForIOS');

    return Ti.Filesystem.directoryForSuite(CONFIG.APP_GROUP);
  };

  /**
   * Obtiene el directorio público
   * @method getSharedPathForAndroid
   * @returns {string} sharedPath
   */
  TokenManager.prototype.getSharedPathForAndroid = function () {
    Ti.API.debug('TokenManager.getSharedPathForAndroid');

    let sharedPath = Ti.Filesystem.applicationDataDirectory;

    if (this.isExternalStoragePresent) {
      sharedPath = Ti.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory);
      sharedPath = sharedPath.getParent().getNativePath() + '/Android/data/' + CONFIG.APP_ID + '/';
    }

    return sharedPath;
  };

  /**
   * Comprueba si el token de refresco ha expirado
   * @method isExpired
   * @returns {boolean} expired
   */
  TokenManager.prototype.isExpired = function () {
    Ti.API.debug('TokenManager.isExpired');

    let expired = true;

    const tokenData = this.readFile(CONFIG.TOKEN_FILENAME);

    if (tokenData && tokenData.refreshTokenTTL && (Date.now() < tokenData.refreshTokenTTL)) {
      expired = false;
    }

    return expired;
  };

  /**
   * Token getter
   * @method getToken
   * @returns {string}
   */
  TokenManager.prototype.getToken = function () {
    Ti.API.debug('TokenManager.getToken');
    return this.token;
  };

  /**
   * Token Setter
   * @method setToken
   * @param   {string} token
   */
  TokenManager.prototype.setToken = function (token) {
    Ti.API.debug('TokenManager.setToken');
    this.token = token;
  };

  /**
   * TTL getter
   * @method getTTL
   * @returns {string}
   */
  TokenManager.prototype.getTTL = function () {
    Ti.API.debug('TokenManager.getTTL');
    return this.ttl;
  };

  /**
   * TTL Setter
   * @method setTTL
   * @param   {number} ttl
   */
  TokenManager.prototype.setTTL = function (ttl) {
    Ti.API.debug('TokenManager.setTTL');
    this.ttl = ttl;
  };

  return TokenManager;

})();
