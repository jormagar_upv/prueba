/**
 * Push Manager Module
 * @module pushManager
 * @description Interfaz de uso para notificaciones push
 * @author Jorge Macías García
 * @version 1.0.0
 */

'use strict';

var Cloud,
  CloudPush,
  PlayServices,
  pushManager,
  params,
  callback,
  platform,
  language,
  i18n,
  requestResetBadge,
  NEXT_RESET_BADGE_REQUEST,
  DEFAULT_LANGUAGE,
  GOOGLE_PLAY_URI;

NEXT_RESET_BADGE_REQUEST = 30000; //30 seconds
DEFAULT_LANGUAGE = 'es';
GOOGLE_PLAY_URI = 'market://details?id=com.google.android.gms';

Cloud = require('ti.cloud');

platform = 'ios';

if (OS_ANDROID) {
  CloudPush = require('ti.cloudpush');
  PlayServices = require('ti.playservices');

  platform = 'android';
}

pushManager = {};

language = DEFAULT_LANGUAGE;

i18n = {
  es: {
    RESULT_SERVICE_INVALID: 'La versión instalada de Google Play Services no es auténtica. Por favor reemplazala',
    RESULT_SERVICE_MISSING: 'Google Play Services no está instalada en el dispositivo. Por favor instálala',
    RESULT_SERVICE_VERSION_UPDATE_REQUIRED: 'Google Play Services está desactualizado. Por favor actualiza',
    RESULT_SERVICE_UPDATING: 'Los servicios de Google Play se están actualizando en este dispositivo.'
  },
  en: {
    RESULT_SERVICE_INVALID: 'The version of Google Play services installed on this device is not authentic. Please replace it',
    RESULT_SERVICE_MISSING: 'Google Play services is not installed on the device. Please install it',
    RESULT_SERVICE_VERSION_UPDATE_REQUIRED: 'Google Play Services is out of date. Please update to latest',
    RESULT_SERVICE_UPDATING: 'Google Play services is currently being updated on this device.'
  },
  ca: {
    RESULT_SERVICE_INVALID: 'La versió instal·lada de Google Play Services no és autèntica. Per favor substitueix-la',
    RESULT_SERVICE_MISSING: 'Google Play Services no està instal·lada al dispositiu. Per favor instal·la-la',
    RESULT_SERVICE_VERSION_UPDATE_REQUIRED: 'Google Play Services està desactualitzat. Per favor actualitza',
    RESULT_SERVICE_UPDATING: 'Els servicis de Google Play s\'estan actualitzant en este dispositiu.'
  }
};

callback = {
  retrieveDeviceToken: {
    success: null,
    error: null
  },
  channel: {
    subscribe: null,
    unsubscribe: null
  },
  receivePush: null
};

requestResetBadge = getRequestResetBadge();

/**
 * init
 * @description Inicializa librería
 * @param {Object} params Parámetros de entrada de la librería
 * @returns {Boolean} isInitialized
 */
function init(params) {
  Ti.API.debug('init on pushManager.js');

  var isInitialized;

  isInitialized = false;

  if (validateParams(params)) {
    Ti.API.debug('pushManager initialized on pushManager.js');
    isInitialized = true;

    language = params.language || DEFAULT_LANGUAGE;
    callback.retrieveDeviceToken.success = params.retrieveDeviceToken.success;
    callback.retrieveDeviceToken.error = params.retrieveDeviceToken.error;
    callback.receivePush = params.receivePush;
    callback.channel.subscribe = params.channel.subscribe;
    callback.channel.unsubscribe = params.channel.unsubscribe;

    if (OS_ANDROID && params.android) {
      setupCloudPush(params.android);
    }
  }

  return isInitialized;
}

/**
 * setupCloudPush
 * @description Inicializa librería para ajustes de Android
 * @param {Object} params Parámetros específicos de Android
 */
function setupCloudPush(params) {
  Ti.API.debug('setAndroidCloudPush on pushManager.js');

  CloudPush.setDebug(params.debug || false);
  CloudPush.setFocusAppOnPush(params.focusAppOnPush || false);
  CloudPush.setShowAppOnTrayClick(params.showAppOnTrayClick || true);
  CloudPush.setShowTrayNotification(params.showTrayNotification || true);
  CloudPush.setShowTrayNotificationsWhenFocused(params.showTrayNotificationsWhenFocused || true);
  CloudPush.setSingleCallback(params.singleCallback || true);
}

/**
 * validateParams
 * @description Validamos que los parámetros de entrada sean correctos
 * @param {Object} params Parámetros de entrada de librería push
 * @returns {Boolean} isValid
 */
function validateParams(params) {
  Ti.API.debug('validateParams on pushManager.js');

  var isValid = false;

  //@formatter:off
  if (Object(params)
    .hasOwnProperty('retrieveDeviceToken') &&
    params.retrieveDeviceToken.hasOwnProperty('success') &&
    typeof params.retrieveDeviceToken.success === 'function' &&
    params.retrieveDeviceToken.hasOwnProperty('error') &&
    typeof params.retrieveDeviceToken.error === 'function' &&
    Object(params)
    .hasOwnProperty('receivePush') &&
    typeof params.receivePush === 'function') {
    //@formatter:on
    isValid = true;
  }

  return isValid;
}

/**
 * retrieveDeviceToken
 * @description Obtiene el token de dispositivo
 */
function retrieveDeviceToken() {
  Ti.API.debug('retrieveDeviceToken on pushManager.js');

  if (OS_IOS) {
    retrieveDeviceTokenOnIOS();
  } else if (OS_ANDROID) {
    retrieveDeviceTokenOnAndroid();
  }
}

/**
 * retrieveDeviceTokenOnIOS
 * @description Obtiene el token de dispositivo para IOS
 */
function retrieveDeviceTokenOnIOS() {
  Ti.API.debug('retrieveDeviceTokenOnIOS on pushManager.js');
  //@formatter:off
  var IOS_NOTIFICATION_TYPES_LIST = [
            Titanium.App.iOS.USER_NOTIFICATION_TYPE_ALERT,
            Titanium.App.iOS.USER_NOTIFICATION_TYPE_SOUND,
            Titanium.App.iOS.USER_NOTIFICATION_TYPE_BADGE
        ];
  //@formatter:on

  // Comprobamos que la versión de iOS >= 8
  if (parseInt(Ti.Platform.version.split('.')[0]) >= 8) {

    // Wait for user settings to be registered before registering for
    // push notifications
    Ti.App.iOS.addEventListener('usernotificationsettings', registerForPushNotificationsOnIOS);

    // Register notification types to use
    Ti.App.iOS.registerUserNotificationSettings({
      types: IOS_NOTIFICATION_TYPES_LIST
    });
  }

  // For iOS 7 and earlier
  else {
    Ti.Network.registerForPushNotifications({
      // Specifies which notifications to receive
      types: IOS_NOTIFICATION_TYPES_LIST,
      success: callback.retrieveDeviceToken.success,
      error: callback.retrieveDeviceToken.error,
      callback: callback.receivePush
    });
  }
}

/**
 * retrieveDeviceTokenOnAndroid
 * @description Obtiene el token de dispositivo para Android
 */
function retrieveDeviceTokenOnAndroid() {
  Ti.API.debug('retrieveDeviceTokenOnAndroid on pushManager.js');

  var isGooglePlayServicesAvailable,
    downloadGooglePlayServices,
    dialog,
    msg;

  isGooglePlayServicesAvailable = PlayServices.isGooglePlayServicesAvailable();

  //Si tenemos disponible Google Play Services solicitamos token de dispositivo

  switch (isGooglePlayServicesAvailable) {
    case PlayServices.RESULT_SERVICE_INVALID:
      msg = i18n[language].RESULT_SERVICE_INVALID;
      downloadGooglePlayServices = true;
      break;

    case PlayServices.RESULT_SERVICE_MISSING:
      msg = i18n[language].RESULT_SERVICE_MISSING;
      downloadGooglePlayServices = true;
      break;

    case PlayServices.RESULT_SERVICE_VERSION_UPDATE_REQUIRED:
      msg = i18n[language].RESULT_SERVICE_VERSION_UPDATE_REQUIRED;
      downloadGooglePlayServices = true;
      break;

    case PlayServices.RESULT_SERVICE_UPDATING:
      msg = i18n[language].RESULT_SERVICE_UPDATING;
      downloadGooglePlayServices = false;
      break;

    case PlayServices.RESULT_SUCCESS:
      CloudPush.retrieveDeviceToken({
        success: callback.retrieveDeviceToken.success,
        error: callback.retrieveDeviceToken.error
      });

      CloudPush.addEventListener('callback', callback.receivePush);
      break;
  }

  if (msg) {
    dialog = Ti.UI.createAlertDialog({
      cancel: 0,
      buttonNames: ['OK'],
      message: msg,
      title: 'Google Play Services',
      downloadGooglePlayServices: downloadGooglePlayServices
    });

    dialog.addEventListener('click', isGooglePlayServicesAvailableDialogCallback);

    dialog.show();
  }
}

/**
 * isGooglePlayServicesAvailableDialogCallback
 * @description Callback de dialogo de aviso sobre Google Services, en caso
 * de necesitar actualizar o descargar Google Play Services abre el navegador
 * @param {Object} event
 */
function isGooglePlayServicesAvailableDialogCallback(event) {
  Ti.API.debug('isGooglePlayServicesAvailableDialogCallback on detail.js');
  var downloadGooglePlayServices;

  downloadGooglePlayServices = event.source.downloadGooglePlayServices;

  if (downloadGooglePlayServices) {
    Ti.Platform.openURL(GOOGLE_PLAY_URI);
  }
}

/**
 * registerForPushNotificationsOnIOS
 * @description Registro para notificaciones en IOS
 */
function registerForPushNotificationsOnIOS() {
  Ti.API.debug('registerForPushNotificationsOnIOS on pushManager.js');

  // Remove event listener once registered for push notifications
  Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPushNotificationsOnIOS);

  Ti.Network.registerForPushNotifications({
    success: callback.retrieveDeviceToken.success,
    error: callback.retrieveDeviceToken.error,
    callback: callback.receivePush
  });
}

/**
 * subscribeToChannel
 * @description Suscripción a un canal de notificaciones
 * @param {Object} deviceToken
 * @param {String} channel
 */
function subscribeToChannel(deviceToken, channel) {
  Ti.API.debug('subscribeToChannel on pushManager.js');

  // Subscribes the device to the 'news_alerts' channel
  // Specify the push type as either 'android' for Android or 'ios' for iOS
  Cloud.PushNotifications.subscribeToken({
    device_token: deviceToken,
    channel: channel,
    type: platform
  }, callback.channel.subscribe);
}

/**
 * unsubscribeToChannel
 * @description Eliminar suscripción a un canal de notificaciones
 * @param {Object} deviceToken
 * @param {String} channel
 */
function unsubscribeToChannel(deviceToken, channel) {
  Ti.API.debug('unsubscribeToChannel on pushManager.js');

  Cloud.PushNotifications.unsubscribeToken({
    device_token: deviceToken,
    channel: channel
  }, callback.channel.unsubscribe);
}

/**
 * resetBadge
 * @description Resetea el contador de notificaciones local
 */
function resetBadge() {
  Ti.API.debug('resetBadge on pushManager.js');

  if (OS_IOS) {
    try {
      Ti.UI.iOS.appBadge = 0;
      Ti.UI.iPhone.appBadge = 0;
    } catch (e) {
      Ti.API.debug(e);
    }
  }
}

/**
 * requestResetBadge
 * @description Resetea de forma remota el badge en UPV
 * @param {string} appId ID de aplicación
 * @param {string} url Endpoint remoto resetBadge
 * @param {string} channel Canal de subscripción
 * @param {string} deviceToken Token push de dispositivo
 * @return {void}
 */
function getRequestResetBadge() {
  var allowNextRequest = 0;

  return function requestResetBadge(appId, url, channel, deviceToken) {
    Ti.API.debug('requestResetBadge on pushManager.js');

    var httpClient, date;

    if (OS_IOS && (new Date())
      .getTime() > allowNextRequest) {

      allowNextRequest = (new Date())
        .getTime() + NEXT_RESET_BADGE_REQUEST;

      httpClient = Ti.Network.createHTTPClient({
        timeout: 5000,
        onload: requestResetBadgeSuccess,
        error: requestResetBadgeError
      });

      httpClient.open('GET', url.replace('{p_devicetoken}', deviceToken)
        .replace('{p_app}', appId)
        .replace('{p_canal}', channel));

      httpClient.send();
    }
  };
}

/**
 * requestResetBadgeSuccess
 * @description Callback éxito reset badge remoto
 * @param {Object} e
 * @return {void}
 */
function requestResetBadgeSuccess(e) {
  Ti.API.debug('requestResetBadgeSuccess');
}

/**
 * requestResetBadgeError
 * @description Callback error reset badge remoto
 * @param {Object} e
 * @returns {void}
 */
function requestResetBadgeError(e) {
  Ti.API.debug('requestResetBadgeError');
}

/** Inicializa la librería */
exports.init = init;

/** Obtiene el token de dispositivo */
exports.retrieveDeviceToken = retrieveDeviceToken;

/** Suscripción a un canal de notificaciones */
exports.subscribeToChannel = subscribeToChannel;

/** Desuscripción de un canal de notificaciones */
exports.unsubscribeToChannel = unsubscribeToChannel;

/** Resetea el contador local de notificaciones */
exports.resetBadge = resetBadge;

/** Resetea el badge en servidor UPV */
exports.requestResetBadge = requestResetBadge;
