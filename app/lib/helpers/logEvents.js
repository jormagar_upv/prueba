/*
 * logEvents.js
 * @description Helper para la gestión de eventos y su registro
 * en el servidor de eventos
 * @author Jorge Macías García
 * @version 1.0.0
 */
var HttpClient,
  httpClient,
  ATTEMPTS,
  METHOD,
  TIMEOUT;

ATTEMPTS = 2;
METHOD = 'POST';
TIMEOUT = 5000;

HttpClient = require('httpclient/httpClient');

/**
 * send
 * @description Envía un evento al servidor pasando un objeto de registro
 * @param {Object} params Objeto de registro de evento
 * params = {
 *  url: 'URL API REST registro de eventos',
 *  headers: 'Objeto headers',
 *  attempts: Número de intentos si falla,
 *  timeout: Timeout,
 *  data: {
 *    p_type: 'Tipo de error',
 *    p_app: 'App ID',
 *    p_url: 'URL generación error (Opcional)',
 *    p_msg: 'Mensaje del evento'
 *  }
 * }
 */
function send(params) {
  var httpClient = new HttpClient();

  if (httpClient.init(getHttpConfig(params))) {
    //httpClient.doRequest();
  } else {
    Ti.API.debug('httpClient init error: ' + JSON.stringify(params));
  }
}

/**
 * Prepara el objeto de configuración para httpClient
 * @param  {Object} params Objeto de configuración proporcionado por el usuario
 * @returns {Object}        Objeto de configuracion para httpClient
 */
function getHttpConfig(params) {
  return {
    method: params.method || METHOD,
    url: params.url,
    success: success,
    error: error,
    attempts: params.attempts || ATTEMPTS,
    headers: params.headers || {},
    data: params.data || {},
    timeout: params.timeout || TIMEOUT
  };
}

/**
 * success
 * Callback Éxito en registro de eventos
 * @param  {Object} httpClient Objeto httpClient
 */
function success(httpClient) {
  Ti.API.debug('Event registered: ' + httpClient.getStatus());
}

/**
 * error
 * Callback error en registro de eventos
 * @param  {Object} httpClient Objeto httpClient
 */
function error(httpClient) {
  if (httpClient) {
    Ti.API.debug('Event not registered: ' + httpClient.getStatus());
  } else {
    Ti.API.debug('Event not registered: httpClient missing');
  }
}

exports.send = send;
