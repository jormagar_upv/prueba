/**
 * Event Manager
 * @module eventManager
 * @description Helper for manage app level events
 * @author Jorge Macías García
 * @version 1.0.0
 */

'use strict';

var eventList = {};

/**
 * addListener
 * @description Add app event listener to event manager.
 * @param {String} event Ti.App event
 * @param {Object} listener Object with controller name and callback
 */
function addListener(event, listener) {
  Ti.API.debug('eventManager_addListener ' + event);

  if (!eventList.hasOwnProperty(event)) {
    eventList[event] = [];
  }

  eventList[event].push(listener);

  Ti.App.addEventListener(event, listener.callback);
}

/**
 * removeListenerFromController
 * @description Remove controller listener from eventManager.
 * @param {String} event Ti.App event
 * @param {String} controller Name of the controller
 */
function removeListenerFromController(event, controller) {
  Ti.API.debug('removeListenerFromController_eventManager ' + event);

  if (eventList.hasOwnProperty(event)) {
    filterEventList(event, controller);
  }
}

/**
 * removeAllListenerFromController
 * @description Remove all controller listener from eventManager.
 * @param {String} controller Name of the controller
 */
function removeAllListenerFromController(controller) {
  Ti.API.debug('removeAllListenerFromController_eventManager');

  Object.keys(eventList).forEach(function (event) {
    filterEventList(event, controller);
  });
}

/**
 * removeAllListeners
 * @description Remove all listeners from event.
 * @param {String} event Ti.App event
 */
function removeAllListeners(event) {
  Ti.API.debug('removeAllListeners_eventManager ' + event);

  if (eventList.hasOwnProperty(event)) {
    removeListenersForEvent(event);
  }
}

/**
 * clearAllListeners
 * @description Remove all listeners from event manager
 */
function clearAllListeners() {
  Ti.API.debug('clearAllListeners_eventManager');

  Object.keys(eventList).forEach(function (event) {
    removeListenersForEvent(event);
  });
}

/**
 * filterEventList
 * @description Apply filter Array function and removes event listeners.
 * @param {String} event App event
 * @param {String} controller Controller name
 */
function filterEventList(event, controller) {
  eventList[event] = eventList[event].filter(function (listener) {
    if (listener.controller === controller) {
      Ti.API.debug('filterEventList ' + event + ' ' + controller);
      Ti.App.removeEventListener(event, listener.callback);
    } else {
      return true;
    }
  });

  if (eventList[event].length == 0) {
    delete eventList[event];
  }
}

/**
 * removeListenersForEvent
 * @description Remove app events for attached event.
 * @param {String} event App event
 */
function removeListenersForEvent(event) {
  eventList[event].forEach(function (listener) {
    Ti.App.removeEventListener(event, listener.callback);
  });
  delete eventList[event];
}

/**
 * fireEvent
 * @description Fire app event.
 * @param {String} event Ti.App event
 */
function fireEvent(event) {
  Ti.App.fireEvent(event);
}

exports.addListener = addListener;
exports.removeListenerFromController = removeListenerFromController;
exports.removeAllListenersFromController = removeAllListenerFromController;
exports.removeAllListeners = removeAllListeners;
exports.clearAllListeners = clearAllListeners;
exports.fireEvent = fireEvent;
