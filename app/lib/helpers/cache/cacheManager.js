/*
 * CacheManager.js
 * @author Jorge Macías García
 */
'use strict';

/**
 * CacheManager
 * @description Constructor clase CacheManager
 */
function CacheManager() {
  this.isExpiredCache = true;
}

/**
 * getIsExpiredCache
 * @description Obtiene el valor del estado de la caducidad de la caché
 */
CacheManager.prototype.getIsExpiredCache = function () {
  return this.isExpiredCache;
};

/**
 * setIsExpiredCache
 * @description Establece el valor del estado de la caducidad de la caché
 * @param {Boolean} isExpiredCache
 */
CacheManager.prototype.setIsExpiredCache = function (isExpiredCache) {
  this.isExpiredCache = isExpiredCache;
};

/**
 * getDataFromProperties
 * @description Obtiene de memoria de la aplicación datos guardados
 * @param {String} type integer, double, string, bool, list, object
 * @param {String} key Clave para Properties
 * @returns {Object}
 */
CacheManager.prototype.getDataFromProperties = function (type, key) {
  var result;

  result = null;
  type = type.toLowerCase();

  if (typeof type === 'string' && type && typeof key === 'string' && key) {

    switch (type) {
      case 'integer':
        result = Ti.App.Properties.getInt(key);
        break;

      case 'double':
        result = Ti.App.Properties.getDouble(key);
        break;

      case 'string':
        result = Ti.App.Properties.getString(key);
        break;

      case 'bool':
        result = Ti.App.Properties.getBool(key);
        break;

      case 'list':
        result = Ti.App.Properties.getList(key);
        break;

      case 'object':
        result = Ti.App.Properties.getObject(key);
        break;

      default:
        break;
    }

  }

  return result;
};

/**
 * setDataToProperties
 * @description Guarda en memoria de la aplicación la datos
 * @param {String} type integer, double, string, bool, list, object
 * @param {String} key Clave para Properties
 * @param {Object} data Datos a almacenar
 */
CacheManager.prototype.setDataToProperties = function (type, key, data) {
  var result;

  result = null;
  type = type.toLowerCase();

  if (typeof type === 'string' && type && typeof key === 'string' && key && data) {

    switch (type) {
      case 'integer':
        Ti.App.Properties.setInt(key, data);
        break;

      case 'double':
        Ti.App.Properties.setDouble(key, data);
        break;

      case 'string':
        Ti.App.Properties.setString(key, data);
        break;

      case 'bool':
        Ti.App.Properties.setBool(key, data);
        break;

      case 'list':
        Ti.App.Properties.setList(key, data);
        break;

      case 'object':
        Ti.App.Properties.setObject(key, data);
        break;

      default:
        break;
    }

  }

  return result;
};

/**
 * removeDataFromProperties
 * @description Elimina de properties
 * @param {String} key Clave para Properties
 */
CacheManager.prototype.removeDataFromProperties = function (key) {
  Ti.App.Properties.removeProperty(key);
};

/**
 * isExpired
 * @description Comrpueba si hay que renovar la caché
 * @param {string} key
 * @returns {Boolean}
 */
CacheManager.prototype.isExpired = function (key) {
  var isExpiredCache,
    expireTTL,
    now;

  if (typeof key !== 'string') {
    throw new TypeError('isCacheExpired: Type of key not is string', 'networkManager.js');
  }

  isExpiredCache = false;

  expireTTL = this.getDataFromProperties('double', key);
  now = (new Date()).getTime();

  if (!expireTTL || (now > expireTTL)) {
    isExpiredCache = true;
  }

  this.isExpiredCache = isExpiredCache;

  return isExpiredCache;
};

/**
 * setNextTTLForCache
 * @param {Object} key
 * @param {Object} secondsToExpire
 */
CacheManager.prototype.setNextTTLForCache = function (key, secondsToExpire) {
  var now,
    expiresIn;

  if (typeof key !== 'string') {
    throw new TypeError('setNextTTLForCache: Type of key not is string', 'networkManager.js');
  }

  if (typeof daysToExpire !== 'undefined' && typeof daysToExpire !== 'number') {
    throw new TypeError('setNextTTLForCache: Type of daysToExpire not is number', 'networkManager.js');
  }

  //Segundos en expirar o ya caducada
  expiresIn = secondsToExpire || 0;

  //Establecemos el próximo ciclo de vida de caché
  now = (new Date()).getTime();
  this.setDataToProperties('double', key, now + (expiresIn * 1000));
  this.isExpiredCache = false;
};

/**
 * clean
 * @description Limpia la caché
 * @param {String} key
 */
CacheManager.prototype.clean = function (key) {

  if (typeof key !== 'string') {
    throw new TypeError('cleanCache: Type of key not is string', 'networkManager.js');
  }

  this.isExpiredCache = true;
  this.removeDataFromProperties(key);
};

module.exports = CacheManager;
