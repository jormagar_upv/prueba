/**
 * downloadManager.js
 * @description Manager para la descarga de ficheros
 */
'use strict';

var NetworkManager,
  i18n,
  LANGUAGE,
  DIALOG_SHOULD_DOWNLOAD,
  DIALOG_NETWORK_NONE,
  DIALOG_NETWORK_MOBILE,
  APPLICATION_DATA_DIRECTORY,
  APPLICATION_EXTERNAL_STORAGE_DIRECTORY,
  DATA_STORAGE,
  EXTERNAL_STORAGE;

DIALOG_SHOULD_DOWNLOAD = 1;
DIALOG_NETWORK_NONE = 2;
DIALOG_NETWORK_MOBILE = 3;

APPLICATION_DATA_DIRECTORY = Ti.Filesystem.applicationDataDirectory;
APPLICATION_EXTERNAL_STORAGE_DIRECTORY = Ti.Filesystem.externalStorageDirectory;

DATA_STORAGE = 'app-data';
EXTERNAL_STORAGE = 'app-external';

i18n = {
  es: {
    alertTitle: 'Atención',
    download: 'Descargar',
    open: 'Abrir',
    cancel: 'Cancelar',
    downloading: 'Descargando',
    downloadedFile: 'Ya descargaste este archivo',
    ok: 'Vale',
    networkNone: 'No hay conexión. Inténtalo más tarde',
    networkMobile: 'No dispones de WiFi. ¿Descargar de todas maneras?'
  },
  en: {
    alertTitle: 'Pay Attention',
    download: 'Download',
    open: 'Open',
    cancel: 'Cancel',
    downloading: 'Downloading',
    downloadedFile: 'You\'ve already downloaded this file ',
    ok: 'Ok',
    networkNone: 'No network. Please try again later.',
    networkMobile: 'You do not have WiFi. Download anyway?'
  },
  ca: {
    alertTitle: 'Atenció',
    download: 'Descàrrega',
    open: 'Obrir',
    cancel: 'Cancel·la',
    downloading: 'Descarregant',
    downloadedFile: 'Ja vas descarregar aquest arxiu',
    ok: 'Val',
    networkNone: 'No hi ha connexió. Intenta-ho més tard',
    networkMobile: 'No disposes de WiFi. ¿Descarregar de totes maneres?'
  }
};

LANGUAGE = (function () {
  var lang;
  switch (Titanium.Locale.getCurrentLanguage().substr(0, 2)) {
    case 'en':
      lang = 'en';
      break;
    case 'ca':
      lang = 'ca';
      break;
    case 'es':
      lang = 'es';
      break;
    default:
      lang = 'en';
      break;
  }
  return lang;
})();

//Managers
NetworkManager = require('helpers/network/networkManager');

/**
 * DownloadManager
 * @description Constructor de clase
 */
function DownloadManager() {
  Ti.API.debug('DownloadManager Constructor');

  this.networkManager = new NetworkManager();
  this.httpConfig = {};
  this.loader = null;
  this.file = {
    storage: null,
    directory: null,
    filename: null,
    handler: null,
    isFileDownloaded: {
      ready: false,
      i18n: null,
      callback: null
    }
  };
  this.callback = {
    success: null,
    error: null
  };
}

/**
 * init
 * @description Inicializa parámetros de librería
 * @param {Object} params
 * @returns {Boolean}
 */
DownloadManager.prototype.init = function (params) {
  Ti.API.debug('DownloadManager init');

  if (!this.validateParams(params)) {
    return false;
  }

  this.setHttpConfigSettings(params.httpConfig);
  this.setFileSettings(params.file);
  this.setIsFileDownloadedSettings(params.isFileDownloaded);
  this.setLoader(params.loader);

  return true;
};

/**
 * validateParams
 * @description Valida la entrada de parametros de la descarga
 * @param {Object} params
 * @returns {Boolean}
 */
DownloadManager.prototype.validateParams = function (params) {
  Ti.API.debug('DownloadManager validateParams');

  /*
  var required = [
       {
           key : 'httpConfig',
           type : 'object'
       },{
           key: 'file',
           type : 'object',
           required: [
          {
                   key: ' size',
                   type : 'number'
               }
           ]
       }
  ];

  function validate(required, obj){
       return required.every(function(element){
           return (typeof this[element.key] === element.type) &&
  			(!element.required || validate(element.required, this[element.key]));
       }, Object(obj));
  }
   */

  if (!params) {
    return false;
  }

  if (!params.hasOwnProperty('httpConfig')) {
    return false;
  }

  if (!params.httpConfig.hasOwnProperty('success') || typeof params.httpConfig.success !== 'function') {
    return false;
  }

  if (!params.httpConfig.hasOwnProperty('error') || typeof params.httpConfig.error !== 'function') {
    return false;
  }

  if (!params.hasOwnProperty('file')) {
    return false;
  }

  if (!params.file.hasOwnProperty('storage') || typeof params.file.storage !== 'string') {
    return false;
  }

  if (!params.file.hasOwnProperty('filename') || typeof params.file.storage !== 'string') {
    return false;
  }

  return true;
};

/**
 * setHttpConfig
 * @description Establece el objeto httpConfig
 * @param {Object} config
 */
DownloadManager.prototype.setHttpConfigSettings = function (config) {
  Ti.API.debug('DownloadManager setHttpConfig');

  //Guardamos callback de usuario
  this.callback.success = config.success;
  this.callback.error = config.error;

  //Guardamos los parámetros
  this.httpConfig = config;

  //Reemplazamos dichos callbacks por los de la librería
  this.httpConfig.success = this.success.bind(this);
  this.httpConfig.error = this.error.bind(this);
};

/**
 * setFileSettings
 * @description Establece el objeto file
 * @param {Object} config
 */
DownloadManager.prototype.setFileSettings = function (config) {
  Ti.API.debug('DownloadManager setFile');

  function normalizeStorage(storageParam) {
    var storage = DATA_STORAGE;

    if (OS_ANDROID && storageParam === EXTERNAL_STORAGE) {
      storage = EXTERNAL_STORAGE;
    }

    return storage;
  }


  this.file.storage = normalizeStorage(config.storage);
  this.file.directory = config.directory || null;
  this.file.filename = config.filename;
};

/**
 * setIsFileDownloadedSettings
 * @description Establece el objeto isFileDownloaded
 * @param {Object} config
 */
DownloadManager.prototype.setIsFileDownloadedSettings = function (config) {
  Ti.API.debug('DownloadManager setIsFileDownloadedSettings');

  var i18nReady = false,
    callbackReady = false;

  if (typeof config === 'object') {
    if (config.hasOwnProperty('i18n') && typeof config.i18n === 'string') {
      i18nReady = true;
    }

    if (config.hasOwnProperty('callback') && typeof config.callback === 'function') {
      callbackReady = true;
    }

    if (i18nReady && callbackReady) {
      this.file.isFileDownloaded = {
        ready: true,
        i18n: config.i18n,
        callback: config.callback
      };
    }
  }
};

DownloadManager.prototype.setLoader = function (loader) {
  this.loader = loader || null;
};

/**
 * download
 * @description Descarga el enlace proporcionado
 * @param {Object} params
 */
DownloadManager.prototype.download = function () {
  Ti.API.debug('DownloadManager download');

  if (this.isFileDownloaded()) {
    this.showDialog(DIALOG_SHOULD_DOWNLOAD);
  } else {
    this.checkNetwork();
  }
};

DownloadManager.prototype.checkNetwork = function () {
  Ti.API.debug('DownloadManager checkNetwork');

  switch (Titanium.Network.networkType) {
    case Titanium.Network.NETWORK_NONE:
      this.showDialog(DIALOG_NETWORK_NONE);
      break;

    case Titanium.Network.NETWORK_MOBILE:
      this.showDialog(DIALOG_NETWORK_MOBILE);
      break;

    default:
      this.downloadFile();
      break;
  }

};

/**
 * isFileDownloaded
 * @description Comprueba si el fichero ya está descargado
 */
DownloadManager.prototype.isFileDownloaded = function () {
  Ti.API.debug('DownloadManager isFileDownloaded');

  var fileHandler,
    exists;

  fileHandler = Ti.Filesystem.getFile(this.getDeviceDirectory(), this.generateFilename());

  exists = fileHandler.exists();

  if (exists) {
    //Guardamos el manejador por si el usuario no
    //quiere volver a descargarlo, de ese modo
    //solo accedemos al sistema de ficheros 1 vez
    this.file.handler = fileHandler;
  }

  return exists;
};

/**
 * downloadFile
 * @description Descarga de fichero
 */
DownloadManager.prototype.downloadFile = function () {
  Ti.API.debug('DownloadManager downloadFile');

  if (this.networkManager.getRemoteData(this.httpConfig)) {
    if (this.loader) {
      this.loader.show(i18n[LANGUAGE].downloading);
    }

  } else {
    Ti.API.debug('Error de inicialización de librería NetworkManager');
    this.callback.error(new Error('Error de inicialización de librería NetworkManager'));
  }
};

/**
 * validateResponse
 * @description Callback éxito descarga fichero
 * @param {Object} httpClient
 */
DownloadManager.prototype.validateResponse = function (httpClient) {
  Ti.API.debug('DownloadManager validateResponse');

  var status,
    responseHeaders;

  return true;
  //Fix temporal: asumimos fichero proporcionado por el usuario

  // Content-Disposition => manda
  // Content-Type: si conozco el MIME pongo la extensión que conozco
  // getContentMIME: En caso de que todo falle, guardamos sin extensión y
  // recuperamos el mimeType del objeto file de titanium

};

/**
 * success
 * @description Callback éxito descarga fichero
 * @param {Object} httpClient
 */
DownloadManager.prototype.success = function (httpClient) {
  Ti.API.debug('DownloadManager success');

  if (this.loader) {
    this.loader.hide();
  }

  if (this.validateResponse(httpClient)) {
    //Devolvemos el fichero
    this.callback.success(this.saveFile(httpClient.getResponseData()));
  } else {
    //Devolvemos error
    this.callback.error(new Error('HTTP ERROR_Respuesta no válida:' + JSON.stringify(httpClient)));
  }
};

/**
 * error
 * @description Callback error descarga fichero
 * @param {Object} httpClient
 */
DownloadManager.prototype.error = function (httpClient) {
  //Pass response dataa
  Ti.API.debug('DownloadManager error');

  var error = {
    type: new Error('Error por defecto'),
    httpClient: null
  };

  if (this.loader) {
    this.loader.hide();
  }

  if (httpClient) {
    error.type = new Error(httpClient.getStatus() + ' - ' + httpClient.getResponseText());
    error.httpClient = httpClient;
  }

  this.callback.error(error);
};

/**
 * saveFile
 * @description Guarda un fichero
 */
DownloadManager.prototype.saveFile = function (data) {
  Ti.API.debug('DownloadManager saveFile');

  var handler,
    deviceDirectory,
    filename;

  deviceDirectory = this.getDeviceDirectory();
  filename = this.getFilename();

  Ti.API.debug('Filename en saveFile ' + filename);

  handler = Ti.Filesystem.getFile(deviceDirectory, filename);
  handler.write(data);

  this.file.handler = handler;

  return handler;
};

/**
 * getDeviceDirectory
 * @description Obtiene el directorio del sistema
 */
DownloadManager.prototype.getDeviceDirectory = function () {
  Ti.API.debug('DownloadManager getDeviceDirectory');

  var deviceDirectory;

  if (OS_IOS) {
    deviceDirectory = this.getDeviceDirectoryOnIOS();
  } else if (OS_ANDROID) {
    deviceDirectory = this.getDeviceDirectoryOnAndroid();
  }

  return deviceDirectory;
};

DownloadManager.prototype.getDeviceDirectoryOnIOS = function () {
  Ti.API.debug('DownloadManager getDeviceDirectoryOnIOS');

  return APPLICATION_DATA_DIRECTORY;
};

DownloadManager.prototype.getDeviceDirectoryOnAndroid = function () {
  Ti.API.debug('DownloadManager getDeviceDirectoryOnAndroid');

  var deviceDirectory;

  switch (this.file.storage) {
    case DATA_STORAGE:
      deviceDirectory = APPLICATION_DATA_DIRECTORY;
      break;

    case EXTERNAL_STORAGE:
      deviceDirectory = APPLICATION_DATA_DIRECTORY;
      //Hay espacio externo
      if (Ti.Filesystem.isExternalStoragePresent()) {
        //Preparar para Android 6.0+
        deviceDirectory = APPLICATION_EXTERNAL_STORAGE_DIRECTORY;

        deviceDirectory = Ti.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory);
        //@formatter:off
        deviceDirectory = deviceDirectory.getParent().getNativePath() +
          '/Android/data/' + Ti.App.getId();
        //@formatter:on
      }
      break;
  }

  return deviceDirectory;
};

/**
 * getFilename
 * @description Obtiene el nombre de un fichero y su carpeta
 */
DownloadManager.prototype.getFilename = function () {
  Ti.API.debug('DownloadManager getFilename');

  var filename;

  filename = this.file.filename;

  if (this.file.directory && this.createDirectory()) {
    filename = this.generateFilename();
  }

  return filename;
};

/**
 * generateFilename
 * @description Obtiene
 */
DownloadManager.prototype.generateFilename = function () {
  Ti.API.debug('DownloadManager generateFilename');
  var filename = this.file.filename;

  if (this.file.directory) {
    filename = [this.file.directory, this.file.filename].join('/');
  }

  return filename;
};

/**
 * createDirectory
 * @param {String} path
 * @param {String} directoryName
 * @returns {Boolean} result
 */
DownloadManager.prototype.createDirectory = function () {
  Ti.API.debug('DownloadManager createDirectory');

  var result,
    directory;

  result = true;

  directory = Titanium.Filesystem.getFile(this.getDeviceDirectory(), this.file.directory);

  if (!directory.exists()) {
    if (directory.createDirectory() === false) {
      directory.deleteDirectory(true);
      result = false;
    }
  }

  return result;
};

/**
 * showDialog
 * @description Muestra un dialogo
 * @param {Integer} dialogType
 */
DownloadManager.prototype.showDialog = function (dialogType) {
  Ti.API.debug('DownloadManager showDialog');

  var dialog,
    dialogParams,
    callback,
    buttonNames;

  callback = null;

  dialogParams = {
    title: i18n[LANGUAGE].alertTitle,
    style: ((OS_IOS) ? Titanium.UI.iPhone.AlertDialogStyle.DEFAULT : null)
  };

  switch (dialogType) {

    case DIALOG_SHOULD_DOWNLOAD:

      //Fix custom callback si el fichero ya existe
      if (this.file.isFileDownloaded.ready) {
        buttonNames = [i18n[LANGUAGE].download, this.file.isFileDownloaded.i18n, i18n[LANGUAGE].cancel];
        callback = 'customAlreadyDownloadedDialogCallback';
      } else {
        buttonNames = [i18n[LANGUAGE].download, i18n[LANGUAGE].cancel];
        callback = 'fileAlreadyDownloadedDialogCallback';
      }

      dialogParams.buttonNames = buttonNames;
      dialogParams.message = i18n[LANGUAGE].downloadedFile;

      break;

    case DIALOG_NETWORK_NONE:
      dialogParams.ok = i18n[LANGUAGE].ok;
      dialogParams.message = i18n[LANGUAGE].networkNone;
      break;

    case DIALOG_NETWORK_MOBILE:
      dialogParams.buttonNames = [i18n[LANGUAGE].download, i18n[LANGUAGE].cancel];
      dialogParams.message = i18n[LANGUAGE].networkMobile;
      callback = 'networkMobileDialogCallback';
      break;
  }

  dialog = Ti.UI.createAlertDialog(dialogParams);

  if (callback) {
    dialog.addEventListener('click', this[callback].bind(this));
  }

  dialog.show();

};

DownloadManager.prototype.customAlreadyDownloadedDialogCallback = function (event) {
  Ti.API.debug('DownloadManager customAlreadyDownloadedDialogCallback');

  if (event.index === 0) {
    //Descargar de nuevo
    this.downloadFile();
  } else if (event.index === 1) {
    //Devolvemos al usuario el manejador para que lo gestione
    this.file.isFileDownloaded.callback(this.file.handler);
  }
};

DownloadManager.prototype.fileAlreadyDownloadedDialogCallback = function (event) {
  Ti.API.debug('DownloadManager shouldDownloadFileCallback');

  if (event.index === 0) {
    //Descargar de nuevo
    this.downloadFile();
  }
};

DownloadManager.prototype.networkMobileDialogCallback = function (event) {
  Ti.API.debug('DownloadManager networkMobileDialogCallback');

  if (event.index === 0) {
    //Descargar de nuevo
    this.downloadFile();
  }
};

module.exports = DownloadManager;
