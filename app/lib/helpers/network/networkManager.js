/*
 * NetworkManager.js
 * @author Jorge Macías García
 */
'use strict';

var HttpClient;

HttpClient = require('httpclient/httpClient');

/**
 * NetworkManager
 * @description Constructor clase NetworkManager
 */
function NetworkManager() {
  this.httpClient = new HttpClient();
  this.isExpiredCache = true;
}

/**
 * getIsExpiredCache
 * @description Obtiene el valor del estado de la caducidad de la caché
 * @returns {boolean}
 */
NetworkManager.prototype.getIsExpiredCache = function () {
  return this.isExpiredCache;
};

/**
 * setIsExpiredCache
 * @description Establece el valor del estado de la caducidad de la caché
 * @param {Boolean} isExpiredCache
 */
NetworkManager.prototype.setIsExpiredCache = function (isExpiredCache) {
  this.isExpiredCache = isExpiredCache;
};

/**
 * getRemoteData
 * @description Obtiene datos remotos de un webservice
 * @param {object} httpConfig
 * @param {String} httpConfig.method
 * @param {String} httpConfig.url
 * @param {Object} httpConfig.success
 * @param {Object} httpConfig.error
 * @param {Number} httpConfig.attempts
 * @param {Number} httpConfig.timeout
 * @param {Object} httpConfig.headers {'header_1':'value_1', 'header_n':'value_n'}
 * @param {Object} httpConfig.data JSON Object as {'key_1':'value_1', 'key_n':'value_n'}
 * @returns {Boolean} error
 */
NetworkManager.prototype.getRemoteData = function (httpConfig) {

  var result;

  result = true;

  if (Titanium.Network.online && this.httpClient.init(httpConfig)) {
    this.httpClient.doRequest();
  } else {
    result = false;
  }

  return result;
};

/**
 * getDataFromProperties
 * @description Obtiene de memoria de la aplicación datos guardados
 * @param {String} type integer, double, string, bool, list, object
 * @param {String} key Clave para Properties
 * @returns {Object}
 */
NetworkManager.prototype.getDataFromProperties = function (type, key) {
  var result;

  result = null;
  type = type.toLowerCase();

  if (typeof type === 'string' && type && typeof key === 'string' && key) {

    switch (type) {
      case 'integer':
        result = Ti.App.Properties.getInt(key);
        break;

      case 'double':
        result = Ti.App.Properties.getDouble(key);
        break;

      case 'string':
        result = Ti.App.Properties.getString(key);
        break;

      case 'bool':
        result = Ti.App.Properties.getBool(key);
        break;

      case 'list':
        result = Ti.App.Properties.getList(key);
        break;

      case 'object':
        result = Ti.App.Properties.getObject(key);
        break;

      default:
        break;
    }

  }

  return result;
};

/**
 * setDataToProperties
 * @description Guarda en memoria de la aplicación la datos
 * @param {String} type integer, double, string, bool, list, object
 * @param {String} key Clave para Properties
 * @param {Object} data Datos a almacenar
 */
NetworkManager.prototype.setDataToProperties = function (type, key, data) {
  var result;

  result = null;
  type = type.toLowerCase();

  if (typeof type === 'string' && type && typeof key === 'string' && key && data) {

    switch (type) {
      case 'integer':
        Ti.App.Properties.setInt(key, data);
        break;

      case 'double':
        Ti.App.Properties.setDouble(key, data);
        break;

      case 'string':
        Ti.App.Properties.setString(key, data);
        break;

      case 'bool':
        Ti.App.Properties.setBool(key, data);
        break;

      case 'list':
        Ti.App.Properties.setList(key, data);
        break;

      case 'object':
        Ti.App.Properties.setObject(key, data);
        break;

      default:
        break;
    }

  }

  return result;
};

/**
 * isCacheExpired
 * @description Comrpueba si hay que renovar la caché
 * @param key
 * @returns {Boolean}
 */
NetworkManager.prototype.isCacheExpired = function (key) {
  var isExpiredCache,
    expireTTL,
    now;

  if (typeof key !== 'string') {
    throw new TypeError('isCacheExpired: Type of key not is string', 'networkManager.js');
  }

  isExpiredCache = false;

  expireTTL = Ti.App.Properties.getDouble(key);
  now = (new Date()).getTime();

  if (!expireTTL || (now > expireTTL)) {
    isExpiredCache = true;
  }

  this.isExpiredCache = isExpiredCache;

  return isExpiredCache;
};

/**
 * setNextTTLForCache
 * @param {Object} key
 * @param {Object} secondsToExpire
 */
NetworkManager.prototype.setNextTTLForCache = function (key, secondsToExpire) {
  var now,
    expiresIn;

  if (typeof key !== 'string') {
    throw new TypeError('setNextTTLForCache: Type of key not is string', 'networkManager.js');
  }

  if (typeof daysToExpire !== 'undefined' && typeof daysToExpire !== 'number') {
    throw new TypeError('setNextTTLForCache: Type of daysToExpire not is number', 'networkManager.js');
  }

  expiresIn = secondsToExpire || 86400;

  //Establecemos el próximo ciclo de vida de caché
  now = (new Date()).getTime();
  this.setDataToProperties('double', key, now + (expiresIn * 1000));
  this.isExpiredCache = false;
};

/**
 * cleanCache
 * @description Limpia la caché
 * @param {String} key
 */
NetworkManager.prototype.cleanCache = function (key) {

  if (typeof key !== 'string') {
    throw new TypeError('cleanCache: Type of key not is string', 'networkManager.js');
  }

  this.isExpiredCache = true;
  Ti.App.Properties.removeProperty(key);
};

NetworkManager.prototype.reset = function () {
  this.httpClient.release();
};

module.exports = NetworkManager;
