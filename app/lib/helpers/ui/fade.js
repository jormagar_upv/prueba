const FADE_DURATION = 750;

exports.setContent = setContent;

/**
 * Establecemos contenido en el contenedor
 * @method setContent
 * @param  {object}   container   Vista contenedora
 * @param  {object}   view   Vista a mostrar
 * @param  {boolean}   reset Flag eliminar contenido
 * @param  {object}   obj Controlador y evento a lanzar
 */
function setContent(container, view, reset, obj) {
  if (reset) {
    removeContent(container, view, obj);
  } else {
    container.add(view);
  }
}

/**
 * @method removeContent
 * @description Establecemos contenido en el contenedor
 * @param  {object}   container   Vista contenedora
 * @param {Object} view
 * @param {Object} obj Controlador y evento a lanzar
 */
function removeContent(container, view, obj) {
  fadeOut(container, container.getChildren()[0], view, obj);
}

/**
 * Inicia el proceso de desaparecer en pantalla
 * @method fadeOut
 * @param {object} container   Vista contenedora
 * @param {object} fadeOutView Vista fade out
 * @param {object} fadeInView Vista fade in
 * @param {object} obj Controlador y evento a lanzar
 */
function fadeOut(container, fadeOutView, fadeInView, obj) {
  var animation;

  animation = Ti.UI.createAnimation({
    opacity: 0,
    duration: FADE_DURATION
  });

  animation.addEventListener('complete', fadeIn.bind(null, container, fadeInView, obj));

  fadeOutView.animate(animation);
}

/**
 * Inicia el proceso de hacer aparecer en pantalla
 * @method fadeIn
 * @param {object} container
 * @param {object} fadeInView
 * @param {object} obj
 * @param  {object} e Evento
 */
function fadeIn(container, fadeInView, obj, e) {
  var animation;

  animation = Ti.UI.createAnimation({
    opacity: 1,
    duration: FADE_DURATION
  });

  fadeInView.setOpacity(0);

  if (Object(obj).hasOwnProperty('e')) {
    animation.addEventListener('complete', fireEvent.bind(null, obj));
  }

  container.removeAllChildren();
  container.add(fadeInView);

  fadeInView.animate(animation);
}

/**
 * Lanza evento
 * @method  fireEvent
 * @param {object} obj
 * @param   {object}  e Evento
 */
function fireEvent(obj, e) {
  obj.controller.trigger(obj.e);
}
