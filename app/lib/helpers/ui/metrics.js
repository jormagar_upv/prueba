const METRICS = getDeviceMetrics();

Ti.API.debug('metrics lib: ' + JSON.stringify(METRICS));

/**
 * Obtiene las métricas del dispositivo
 * @method  getDeviceMetrics
 * @returns {object} METRICS
 */
function getDeviceMetrics() {
  const APP_BAR = OS_IOS ? 44 : 56;
  const STATUS_BAR = OS_IOS ? 20 : 24;
  const WINDOW = APP_BAR + STATUS_BAR;
  const DPI_FACTOR = (Ti.Platform.displayCaps.dpi / 160);

  const metrics = {
    WIDTH: OS_IOS ? Ti.Platform.displayCaps.getPlatformWidth() : (Ti.Platform.displayCaps.getPlatformWidth() / DPI_FACTOR),
    HEIGHT: OS_IOS ? Ti.Platform.displayCaps.getPlatformHeight() : (Ti.Platform.displayCaps.getPlatformHeight() / DPI_FACTOR)
  };

  if (metrics.WIDTH > metrics.HEIGHT) {
    let aux = metrics.WIDTH;
    metrics.WIDTH = metrics.HEIGHT;
    metrics.HEIGHT = aux;
  }

  metrics.RATIO = (metrics.HEIGHT / metrics.WIDTH);
  metrics.CENTER = ((metrics.HEIGHT - WINDOW) / 2);
  metrics.CENTER_TOP_LOGIN = metrics.CENTER - (79 / DPI_FACTOR);
  metrics.CENTER_BOTTOM_LOGIN = metrics.CENTER + (79 / DPI_FACTOR);
  metrics.SHAPE_LOGIN_HEIGHT = 158 / DPI_FACTOR;

  return metrics;
}

module.exports = {
  METRICS: METRICS,
  HEADER_HEIGHT: METRICS.WIDTH / METRICS.RATIO
};
