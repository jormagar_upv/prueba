var log = require('helpers/log');

var CALENDAR,
  CAMERA,
  CONTACTS,
  LOCATION,
  STORAGE,
  PERMISSION_RESTRICTED,
  PERMISSION_DENIED,
  PERMISSION_LIST_DENIED,
  IOS_EVENTS_AUTHORIZATION;

CALENDAR = 'calendar';
CAMERA = 'camera';
CONTACTS = 'contacts';
LOCATION = 'location';
STORAGE = 'storage';

PERMISSION_RESTRICTED = {
  code: 600,
  message: 'Restricted permission'
};

PERMISSION_DENIED = {
  code: 601,
  message: 'Denied permission'
};

if (OS_IOS) {
  IOS_EVENTS_AUTHORIZATION = {};
  IOS_EVENTS_AUTHORIZATION[Ti.Calendar.AUTHORIZATION_AUTHORIZED] = 'AUTHORIZATION_AUTHORIZED';
  IOS_EVENTS_AUTHORIZATION[Ti.Calendar.AUTHORIZATION_DENIED] = 'AUTHORIZATION_DENIED';
  IOS_EVENTS_AUTHORIZATION[Ti.Calendar.AUTHORIZATION_RESTRICTED] = 'AUTHORIZATION_RESTRICTED';
  IOS_EVENTS_AUTHORIZATION[Ti.Calendar.AUTHORIZATION_UNKNOWN] = 'AUTHORIZATION_UNKNOWN';
}

/**
 * openAppPermissions
 * @description Abrimos ajustes de permisos de la aplicación
 */
function openAppPermissions() {
  log.args('permissionManager_Lib openAppPermissions');

  var intent;

  if (OS_IOS) {
    Ti.Platform.openURL(Ti.App.iOS.applicationOpenSettingsURL);
  }

  if (OS_ANDROID) {

    intent = Ti.Android.createIntent({
      action: 'android.settings.APPLICATION_DETAILS_SETTINGS',
      data: 'package:' + Ti.App.getId()
    });

    intent.addFlags(Ti.Android.FLAG_ACTIVITY_NEW_TASK);

    if (Ti.Android.currentActivity) {
      Ti.Android.currentActivity.startActivity(intent);
    }
  }
}

/**
 * request
 * @description Solicita permiso
 * @param {String} permission Permiso que queremos solicitar
 * @param {Function} success Callback exito
 * @param {Function} error Callback error
 */
function request(permission, success, error) {
  log.args('permissionManager_Lib request');

  switch (permission) {
    case CALENDAR:
      requestCalendar(success, error);
      break;

    case CAMERA:
      requestCamera(success, error);
      break;

    case CONTACTS:
      requestContacts(success, error);
      break;

    case LOCATION:
      requestLocation(success, error);
      break;

    case STORAGE:
      requestStorage(success, error);
      break;

    default:
      error();
      break;
  }
}

/**
 * getAuthorizationState
 * @description Comprobamos si existe error de permisos en iOS
 * @param {String} component
 */
function getAuthorizationState(component) {
  var authorization,
    permissionError,
    api;

  switch (component) {
    case CALENDAR:
      api = 'Calendar';
      authorization = Ti.Calendar.calendarAuthorization;
      break;

    case CAMERA:
      api = 'Media';
      authorization = Ti.Media.cameraAuthorization;
      break;

    case CONTACTS:
      api = 'Contacts';
      authorization = Ti.Contacts.contactsAuthorization;
      break;

    case LOCATION:
      api = 'Geolocation';
      authorization = Ti.Geolocation.locationServicesAuthorization;
      break;

    default:
      break;
  }

  if (authorization === Ti[api].AUTHORIZATION_RESTRICTED) {
    permissionError = PERMISSION_RESTRICTED;
    permissionError.message = 'Restricted permission: ' + component;
  } else if (authorization === Ti[api].AUTHORIZATION_DENIED) {
    permissionError = PERMISSION_DENIED;
    permissionError.message = 'Denied permission: ' + component;
  }

  return permissionError;
}

/**
 * requestCalendar
 * @description Solicitamos permisos de calendario
 * @param {Function} success Callback exito
 * @param {Function} error Callback error
 */
function requestCalendar(success, error) {
  log.args('permissionManager_Lib requestCalendar');

  var hasPermissions,
    permissionError;

  hasPermissions = Ti.Calendar.hasCalendarPermissions();

  if (hasPermissions) {
    success();
  } else {

    if (OS_IOS) {
      permissionError = getAuthorizationState(CALENDAR);
    }

    if (permissionError) {
      error(permissionError);
    } else {
      Ti.Calendar.requestCalendarPermissions(function (e) {
        log.args('Ti.Calendar.requestCalendarPermissions', e);

        if (e.success) {
          success();
        } else {
          permissionError = PERMISSION_DENIED;
          permissionError.message = 'Denied permission: ' + CALENDAR;
          error(permissionError);
        }
      });
    }
  }
}

/**
 * requestContacts
 * @description Solicitamos permisos de contactos
 * @param {Function} success Callback exito
 * @param {Function} error Callback error
 */
function requestContacts(success, error) {
  log.args('permissionManager_Lib requestContacts');

  var hasPermissions,
    permissionError;

  hasPermissions = Ti.Contacts.hasContactsPermissions();

  if (hasPermissions) {
    success();
  } else {

    if (OS_IOS) {
      permissionError = getAuthorizationState(CONTACTS);
    }

    if (permissionError) {
      error(permissionError);
    } else {
      Ti.Contacts.requestContactsPermissions(function (e) {
        log.args('Ti.Contacts.requestContactsPermissions', e);

        if (e.success) {
          success();
        } else {
          permissionError = PERMISSION_DENIED;
          permissionError.message = 'Denied permission: ' + CONTACTS;
          error(permissionError);
        }
      });

    }
  }
}

/**
 * requestLocation
 * @description Solicitamos permisos de geolocalizaci
 * @param {Function} success Callback exito
 * @param {Function} error Callback error
 */
function requestLocation(success, error) {
  log.args('permissionManager_Lib requestLocation');

  var hasPermissions,
    permissionError;

  hasPermissions = Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS);

  if (hasPermissions) {
    success();
  } else {

    if (OS_IOS) {
      permissionError = getAuthorizationState(LOCATION);
    }

    if (permissionError) {
      error(permissionError);
    } else {
      Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS, function (e) {
        log.args('Ti.Geolocation.requestLocationPermissions', e);

        if (e.success) {
          success();
        } else {
          permissionError = PERMISSION_DENIED;
          permissionError.message = 'Denied permission: ' + LOCATION;
          error(permissionError);
        }
      });

    }
  }
}

/**
 * requestCamera
 * @description Solicitamos permisos de camara
 * @param {Function} success Callback exito
 * @param {Function} error Callback error
 */
function requestCamera(success, error) {
  log.args('permissionManager_Lib requestCamera');

  var hasPermissions,
    permissionError;

  hasPermissions = Ti.Media.hasCameraPermissions();

  if (hasPermissions) {
    success();
  } else {

    if (OS_IOS) {
      permissionError = getAuthorizationState(CAMERA);
    }

    if (permissionError) {
      error(permissionError);
    } else {
      Ti.Media.requestCameraPermissions(function (e) {
        log.args('Ti.Geolocation.requestLocationPermissions', e);

        if (e.success) {
          success();
        } else {
          permissionError = PERMISSION_DENIED;
          permissionError.message = 'Denied permission: ' + CAMERA;
          error(permissionError);
        }
      });
    }
  }
}

/**
 * requestStorage
 * @description Solicitamos permisos de escritura
 * @param {Function} success Callback exito
 * @param {Function} error Callback error
 */
function requestStorage(success, error) {
  log.args('permissionManager_Lib requestStorage');

  var hasPermission,
    permissionError;

  if (OS_IOS) {
    success();
  }

  if (OS_ANDROID) {

    hasPermission = Ti.Filesystem.hasStoragePermissions();
    log.args('Ti.Filesystem.requestStoragePermissions', hasPermission);

    if (hasPermission) {
      success();
    } else {
      Ti.Filesystem.requestStoragePermissions(function (e) {
        log.args('Ti.Filesystem.requestStoragePermissions', e);

        if (e.success) {
          success();
        } else {
          permissionError = PERMISSION_DENIED;
          permissionError.message = 'Denied permission: ' + STORAGE;
          error(permissionError);
        }
      });
    }
  }
}

/**
 * requestPermissionList
 * @description Solicitamos lista de permisos
 * @param {Object} permissionList Lista de permisos
 * @param {Function} success Callback exito
 * @param {Function} error Callback error
 */
function requestPermissionList(permissionList, success, error) {
  log.args('permisionManager_Lib requestPermissionList');

  var hasPermission,
    permissionError,
    permissionsToCheck,
    permissionDeniedList;

  if (OS_ANDROID && Array.isArray(permissionList)) {

    permissionsToCheck = getDeniedPermissions(permissionList);

    if (!permissionsToCheck.length) {
      success();
    } else {
      Ti.Android.requestPermissions(permissionsToCheck, function (e) {
        if (e.success) {
          success();
        } else {
          permissionError = PERMISSION_DENIED;
          permissionDeniedList = getDeniedPermissions(permissionList);
          permissionError.message = 'Denied permissions: ' + permissionDeniedList.join(',');
          permissionError.permissions = permissionDeniedList;
          error(permissionError);
        }
      });
    }
  }
}

/**
 * getDeniedPermissions
 * @description Obtiene una lista de permisos no concedidos
 * @param {Object} list
 * @returns {Object} Lista de permisos no otorgados
 */
function getDeniedPermissions(list) {
  return list.filter(function (permission) {
    return !Ti.Android.hasPermission(permission);
  });
}

exports.request = request;
exports.requestPermissionList = requestPermissionList;
exports.openAppPermissions = openAppPermissions;
