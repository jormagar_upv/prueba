module.exports = {
  //"extends": "airbnb-base/legacy",
  "extends": "eslint-config-airbnb-es5",
  "globals": {
    "Ti": false,
    "Titanium": false,
    "Alloy": false,
    "describe": false,
    "it": false,
    "before": false,
    "beforeEach": false,
    "after": false,
    "afterEach": false,
    "should": false,
    "$": false,
    "Backbone": false,
    "OS_IOS": false,
    "OS_ANDROID": false,
    "_": false,
    "ENV_DEV": false,
    "ENV_TEST": false,
    "ENV_PRODUCTION": false,
    "DIST_ADHOC": false,
    "DIST_STORE": false,
    "L": false,
    "alert": false,
    "console": false,
    "Widget": false,
    "WPATH": false,
    "APP": false,
    "$model": false,
    "Helper": true,
    "Manager": true,
    "Events": true,
    "UPV_LOGIN_CONFIG": true
  },
  "rules": {
    "no-prototype-builtins": "off",
    "no-underscore-dangle": ["off"],
    "no-unused-expressions:": ["warning", {
      "allowShortCircuit": true,
      "allowTernary": true
    }],
    "one-var": ["error", {
      "var": "always",
      "let": "never",
      "const": "never"
    }],
    "func-names": "off",
    "space-before-function-paren": ["error", {
      "anonymous": "always",
      "named": "never",
      "asyncArrow": "ignore"
    }],
    "dot-notation": "warn",
    "no-param-reassign": "warn",
    "vars-on-top": "warn",
    "wrap-iife": ["error", "inside"],
    "no-use-before-define": ["error", {
      "functions": false,
      "classes": true
    }],
    "global-require": "off",
    "spaced-comment": "off",
    "no-shadow": ["warn", {
      "builtinGlobals": true,
      "hoist": "all",
      "allow": []
    }],
    "no-console": ["error", {
      "allow": ["warn", "error", "log"]
    }],
    "no-alert": "off",
    "no-unused-vars": ["warn", {
      "vars": "all",
      "args": "none",
      "ignoreRestSiblings": false
    }],
    "max-len": ["error", {
      "code": 180,
      "tabWidth": 2,
      "ignoreUrls": true
    }],
    "new-cap": ["warn", {
      "newIsCap": true,
      "capIsNewExceptions": ["L"]
    }],
    "valid-jsdoc": ["error", {
      "prefer": {
        "arg": "param",
        "argument": "param",
        "class": "constructor",
        "return": "returns",
        "virtual": "abstract"
      },
      "requireReturn": false,
      "requireReturnDescription": false,
      "requireParamDescription": false
    }],
    "indent": ["error", 2, {
      "MemberExpression": 1,
      "SwitchCase": 1
    }],
    "padded-blocks": "off",
    "no-native-reassign": ["error", {
      "exceptions": ["$"]
    }],
    "eqeqeq": ["error", "smart"],
    "no-extend-native": "off"
  }
};
